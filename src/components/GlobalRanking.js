import React, {Component} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';

export default class GlobalRanking extends Component {
  render() {
    return (
      <View testID="globalRankCard" style={styles.cardContainer}>
        <View style={styles.cardInterior}>
          <View style={styles.logoCard}>
            <Image
              testID="companyLogo"
              source={require('../assets/images/merck.png')}
              style={styles.companyLogo}
            />
          </View>
          <View style={styles.rankCard}>
            <View style={styles.rankScore}>
              <Text testID="companyRank" style={styles.companyScore}>
                127
              </Text>
              <Text style={styles.outOf}>/</Text>
              <Text testID="totalRank" style={styles.totalScore}>
                2512
              </Text>
            </View>
            <View style={{flexDirection: 'row', justifyContent: 'center'}}>
              <Text testID="globalRank" style={styles.rankTitle}>
                Globally Benchmark Rank
              </Text>
            </View>
          </View>
          <View style={styles.detailsParent}>
            <View>
              <Text testID="securityPosture" style={styles.details}>
                Security Posture
              </Text>
              <View></View>
              <Text testID="securityPostureValue" style={styles.ranking}>
                75%
              </Text>
            </View>
            <View style={{width: 1, backgroundColor: 'rgb(210,210,210)'}} />
            <View>
              <Text testID="healthcareIndustry" style={styles.details}>
                Healthcare Industry
              </Text>
              <View
                testID="healthcareIndustryValue"
                style={{flexDirection: 'row'}}>
                <Text style={styles.ranking}>84</Text>
                <Text style={{color: '#06b5c6'}}>th</Text>
              </View>
            </View>
          </View>
          <View style={{flexDirection: 'row', paddingTop: 15}}>
            <View>
              <Text testID="company" style={styles.names}>
                Company
              </Text>
              <Text testID="domain" style={styles.names}>
                Domain
              </Text>
              <Text testID="industry" style={styles.names}>
                Industry
              </Text>
              <Text testID="strength" style={styles.names}>
                Strength
              </Text>
            </View>
            <View style={{marginLeft: 25}}>
              <Text testID="companyValue" style={styles.nameDetails}>
                Merck, Santa Clara, CA
              </Text>
              <Text testID="domainValue" style={styles.nameDetails}>
                www.merck.com
              </Text>
              <Text testID="industryValue" style={styles.nameDetails}>
                Healthcare
              </Text>
              <Text testID="strengthValue" style={styles.nameDetails}>
                100-500
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexBasis: '8%',
  },
  cardInterior: {
    borderColor: '#ededed',
    borderWidth: 2,
    borderRadius: 4,
    padding: 16,
  },
  logoCard: {
    paddingTop: 10,
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 2,
    backgroundColor: '#f5f9f3',
    borderColor: '#cfe5c1',
    borderLeftWidth: 2,
    borderRightWidth: 2,
    borderTopWidth: 2,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    alignSelf: 'center',
    position: 'relative',
    top: 2,
    zIndex: 2,
    flexDirection: 'row',
  },
  rankCard: {
    backgroundColor: '#f5f9f3',
    borderColor: '#cfe5c1',
    borderWidth: 2,
    borderRadius: 4,
    padding: 10,
  },
  companyLogo: {
    width: 100,
    height: 30,
  },
  rankScore: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingHorizontal: 5,
    paddingTop: 5,
  },
  companyScore: {
    fontSize: 26,
    fontFamily: 'Poppins-Bold',
    letterSpacing: 0.4,
    color: '#4e852a',
  },
  outOf: {
    fontSize: 26,
    color: '#3b3c3b',
  },
  totalScore: {
    color: '#3b3c3b',
    fontSize: 20,
    fontFamily: 'Poppins-Regular',
    marginTop: 6,
    marginLeft: 2,
  },
  rankTitle: {
    fontSize: 17,
    color: 'rgb(43,43,43)',
    fontFamily: 'Poppins-SemiBold',
    paddingBottom: 0,
  },
  detailsParent: {
    paddingTop: 15,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  details: {
    color: '#3b3c3b',
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
  ranking: {
    color: '#06b5c6',
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
  },
  names: {
    color: '#4c4c4c',
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
    lineHeight: 24,
  },
  nameDetails: {
    color: 'rgb(43,43,43)',
    fontSize: 12,
    marginTop: 1,
    fontFamily: 'Poppins-Medium',
    lineHeight: 23,
  },
});
