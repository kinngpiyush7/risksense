describe('Test for Subcription plan page', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('subcription box page', async () => {
    await expect(element(by.id('SP_backarrow_button'))).toBeVisible();
    await element(by.id('SP_backarrow_button')).tap();
    await expect(element(by.id('SP_backarrow'))).toBeVisible();
    await expect(element(by.id('SP_heading'))).toBeVisible();
    await expect(element(by.id('SP_heading'))).toHaveText(
      'Select a Scan Report Subcription Plan',
    );
    await expect(element(by.id('SP_sub_heading'))).toBeVisible();
    await expect(element(by.id('SP_sub_heading'))).toHaveText(
      'Security rating report each for each scoring component and the overall organizational score',
    );

    await expect(element(by.id('SB_inner_tableheading'))).toHaveText(
      'Report Features',
    );
  });
});
