import React, {Component} from 'react';
import {Text, StyleSheet, Dimensions} from 'react-native';
import {SafeAreaView} from 'react-navigation';

export default class Footer extends Component {
  render() {
    return (
      <SafeAreaView style={styles.footer}>
        <Text testID="footer" style={styles.footer_text}>
          Terms and Conditions
        </Text>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  footer: {
    width: Dimensions.get('window').width,
    backgroundColor: '#3E3C3D',
    paddingVertical: 20,
  },
  footer_text: {
    fontFamily: 'Poppins-Regular',
    color: '#D2D2D2',
    marginLeft: 'auto',
    marginRight: 55,
    fontSize: 13,
  },
});
