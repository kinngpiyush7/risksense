import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import Pagination from './Pagination';
import {Table, Row, Rows} from 'react-native-table-component';

const ReportTable = props => {
  return (
    <View>
      <View
        style={[styles.table, props.tableMargin ? props.tableMargin : null]}>
        <View>
          <Text
            testID="RT_tableHeading"
            style={{
              fontFamily: 'Poppins-Regular',
              fontSize: 13,
              paddingLeft: 15,
              marginBottom: props.showLegend ? null : 40,
            }}>
            {props.heading}
          </Text>
        </View>
        {props.showLegend && (
          //for Legend on the table
          <View testID="RT_tableLegends" style={styles.table_legends}>
            <View style={{padding: 10}}>
              <View style={styles.Crtical}></View>
              <Text
                style={{
                  color: '#4A4A4A',
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                Critical
              </Text>
            </View>
            <View style={{padding: 10}}>
              <View style={styles.High}></View>
              <Text
                style={{
                  color: '#4A4A4A',
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                High
              </Text>
            </View>
            <View style={{padding: 10}}>
              <View style={styles.Medium}></View>
              <Text
                style={{
                  color: '#4A4A4A',
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                Medium
              </Text>
            </View>
            <View style={{padding: 10}}>
              <View style={styles.Low}></View>
              <Text
                style={{
                  color: '#4A4A4A',
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                Low
              </Text>
            </View>
            <View style={{padding: 10}}>
              <View style={styles.informatinal}></View>
              <Text
                style={{
                  color: '#4A4A4A',
                  fontFamily: 'Poppins-Regular',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                Informational
              </Text>
            </View>
          </View>
        )}

        <View style={styles.container}>
          <Table>
            {/* this for the table colume headers */}
            <Row
              data={props.column_names}
              style={styles.head}
              textStyle={styles.headtext}
              widthArr={props.widthArr}
            />
            {/* this for thye table row data */}

            <Rows
              data={props.column_data}
              widthArr={props.widthArr}
              textStyle={styles.datatext}
              style={styles.columndata}
            />
            <View>
              {props.totalRows >= 10 ? (
                <View>
                  {/* here we call teh pagination reuseable compnent */}
                  <Pagination
                    RowPerPage={props.RowPerPage}
                    totalRows={props.totalRows}
                    handlePageChange={props.handlePageChange}
                    CurrentPage={props.CurrentPage}
                  />
                </View>
              ) : null}
            </View>
          </Table>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  table: {
    backgroundColor: '#F7F7F7',
    padding: 15,
    margin: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#D2D2D2',
  },
  table_legends: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginBottom: 20,
  },
  Crtical: {
    height: 4,
    width: 20,
    backgroundColor: '#D53E4F',
    borderRadius: 15,
  },
  High: {
    height: 4,
    width: 20,
    backgroundColor: '#F46D43',
    borderRadius: 20,
  },
  Medium: {
    height: 4,
    width: 20,
    backgroundColor: '#FDAE61',
    borderRadius: 20,
  },
  Low: {
    height: 4,
    width: 20,
    backgroundColor: '#FFC736',
    borderRadius: 20,
  },
  informatinal: {
    height: 4,
    width: 20,
    backgroundColor: '#00B3C4',
    borderRadius: 20,
  },

  container: {},
  head: {
    fontSize: 14,
    paddingLeft: 15,
  },
  headtext: {
    color: '#8C8C8C',
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
  },
  datatext: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
  },
  columndata: {
    borderBottomWidth: 1,
    borderBottomColor: '#E2E2E2',
    paddingVertical: 12,
    paddingLeft: 15,
  },
});

export default ReportTable;
