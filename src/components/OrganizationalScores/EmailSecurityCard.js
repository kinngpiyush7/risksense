import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import ScoresHeading from './ScoresHeading';
import TagCards from './TagCards';
import Gauge from './Gauge';
import CircularProgress from './CircularProgress';
import CustomButton from '../CustomButton';

import {smallButton, cardButton} from '../../assets/styles/Buttons';

const scoreData = [
  {
    tags: [
      {tagTitle: 'CWE - 10', bgColor: '#9E0A1B', borderColor: '#9E0A1B'},
      {
        tagTitle: 'SMTP - 10',
        bgColor: 'rgb(0,179,196)',
        borderColor: 'rgb(0,179,196)',
      },
    ],
    tagsTitle: 'DNS Sec. KEY rotation period not within recommended range',
  },
  {
    tags: [
      {
        tagTitle: 'SMTP - 213',
        bgColor: 'rgb(251,206,63)',
        borderColor: 'rgb(251,206,63)',
      },
    ],
    tagsTitle: 'ALT2.ASPMX.L.GOOGLE.com PORT 993',
  },
  {
    tags: [
      {
        tagTitle: 'CWE - 10',
        bgColor: 'rgb(255,137,48)',
        borderColor: 'rgb(255,137,48)',
      },
    ],
    tagsTitle: 'ASPMX.L.GOOGLE.com PORT 143',
  },
  {
    tags: [
      {
        tagTitle: 'SMTP - 213',
        bgColor: 'rgb(251,206,63)',
        borderColor: 'rgb(251,206,63)',
      },
    ],
    tagsTitle: 'DKIM Record Not Found',
  },
  {
    tags: [
      {
        tagTitle: 'SMTP - 213',
        bgColor: 'rgb(251,206,63)',
        borderColor: 'rgb(251,206,63)',
      },
    ],
    tagsTitle: 'ALT2.ASPMX.L.GOOGLE.com PORT 993',
  },
];

const EmailSecurityCard = props => {
  return (
    <View style={styles.cardContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 14,
        }}>
        <Text
          testID="emailSecCardText"
          style={{
            fontFamily: 'Poppins-SemiBold',
            color: 'rgb(43,43,43)',
            marginTop: 12,
            fontSize: 14,
          }}>
          Email Security
        </Text>
        <TouchableOpacity onPress={() => props.activeScreen('')}>
          <View style={{paddingHorizontal: 14, paddingVertical: 12}}>
            <Text
              testID="closeIcon"
              style={{
                color: 'rgb(0,179,196)',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              X
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* graphs view */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 6,
          // marginTop: 6,
          marginBottom: 20,
          marginHorizontal: 14,
        }}>
        <CircularProgress maxValue="1335" value="359" small={false} />
        <Gauge gaugeScore="437" gaugeType="Email" />
      </View>
      <View style={{marginHorizontal: 14}}>
        <ScoresHeading headingTextId="emailSecCardHeaderText">
          Top Findings
        </ScoresHeading>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 12,
          }}>
          <Text
            style={{
              fontFamily: 'Poppins-Medium',
              fontSize: 12,
              marginRight: 20,
            }}>
            CWE
          </Text>
          <Text
            testID="emailSecCWE"
            style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
            Common Weakness Enumeration
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 0,
          }}>
          <Text
            style={{
              fontFamily: 'Poppins-Medium',
              fontSize: 12,
              marginRight: 20,
            }}>
            SMTP
          </Text>
          <Text
            testID="emailSecSMTP"
            style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
            Sender Policy Framework
          </Text>
        </View>
        <View style={{height: 354, overflow: 'hidden'}}>
          <FlatList
            style={{marginTop: 12}}
            data={scoreData}
            renderItem={({item, index}) => (
              <TagCards title={item.tagsTitle} tags={item.tags} />
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
      <View style={cardButton}>
        <CustomButton
          buttonTest="emailSecDetailReportButton"
          handleButtonPress={() => props.navigate('emailReport')}
          buttonBackgroundColor={true}
          textColor={false}
          hasShadow={true}
          textStyle={smallButton.buttonTextStyle}
          viewStyle={smallButton.buttonViewStyle}
          navigation={() => {}}>
          Detail Report
        </CustomButton>
      </View>
    </View>
  );
};

export default EmailSecurityCard;

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: '#00b3c4',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    paddingBottom: 14,
    height: '100%',
  },
});
