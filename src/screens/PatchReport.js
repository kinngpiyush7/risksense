import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import ReportTable from '../components/ReportTable';
import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import arrow from '../assets/images/triangle.png';
import LineGraph from '../components/LineGraph';
import CustomButton from '../components/CustomButton';
import Tag from '../components/OrganizationalScores/Tag';
import PR_image from '../assets/images/patchCadence/srsIconsPatchingCadence.png';
import {SafeAreaView} from 'react-navigation';
import {mediumButton} from '../assets/styles/Buttons';
import {widthPercentageToDP} from '../components/Responsive';

export default class PatchReport extends Component {
  state = {CurrentPage: 1, RowPerPage: 10};
  static navigationOptions = {
    header: null,
  };
  handlePageChange = pageNumber => {
    this.setState({CurrentPage: pageNumber});
  };
  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };

  render() {
    // data for the table
    const data = [
      [
        <Tag
          tag={'CVE-2014-3581'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#f2b924',
            borderColor: '#f2b924',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          care2care.dhmh.merck.gov (167.102.229.98)
        </Text>,
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2018-1333'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#00b3c4',
            borderColor: '#00b3c4',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        <Text
          style={{
            color: '#CD3547',
          }}>
          3.5
        </Text>,
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-7679'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#c61227',
            borderColor: '#c61227',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        '5.67',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>167.102.229.110</Text>,
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-7679'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#c61227',
            borderColor: '#c61227',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          167.102.229.42\n167.102.229.110
        </Text>,
        '4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-1333'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#01b3c4',
            borderColor: '#01b3c4',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        '5',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>167.102.229.110</Text>,
        <Text style={{color: '#CD3547'}}>2.56</Text>,
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>167.102.229.110</Text>,
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2014-3581'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#f2b924',
            borderColor: '#f2b924',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          care2care.dhmh.merck.gov (167.102.229.98)
        </Text>,
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2018-1333'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#00b3c4',
            borderColor: '#00b3c4',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        '3.5',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-7679'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#00b3c4',
            borderColor: '#00b3c4',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        '5.67',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        '167.102.229.110',
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-7679'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#c61227',
            borderColor: '#c61227',
          }}
        />,
        '167.102.229.42\n167.102.229.110',
        '4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-1333'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#00b3c4',
            borderColor: '#00b3c4',
          }}
        />,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          immunet.dhmh.merck.gov (167.102.229.6)
        </Text>,
        '5',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        '167.102.229.110',

        <Text style={{color: '#CD3547'}}>2.56</Text>,
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-2017-3169'}
          customText={{fontSize: 11, lineHeight: 13}}
          customStyle={{
            width: 100,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
        />,
        '167.102.229.110',
        '4.4',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
    ];
    //will be use for pagination when we get data from back end
    let filtereddata = data;
    // [Pagination] Fetching the first and last data index
    // and slicing to get current data
    let indexOfLastRow = this.state.CurrentPage * this.state.RowPerPage;
    let indexOfFirstRow = indexOfLastRow - this.state.RowPerPage;
    let current_data =
      filtereddata !== null
        ? filtereddata.slice(indexOfFirstRow, indexOfLastRow)
        : null;
    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="PR_container" style={styles.PR_container}>
            <View testID="PR_header" style={styles.header}>
              <TouchableOpacity
                testID="PR_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="PR_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="PR_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={PR_image}></Image>
              <View testID="PR_header_texts" style={styles.header_texts}>
                <Text testID="PR_heading" style={styles.heading}>
                  Patch Cadence
                </Text>
                <Text testID="PR_sub_heading" style={styles.sub_heading}>
                  Determining no. of vulnerabilities & how many critical
                  vulnerabilities have yet to be patched
                </Text>
              </View>
            </View>
            <View testID="PR_graphcontainer" style={styles.PR_graphcontainer}>
              <View
                testID="PR_graphcontainer_heading"
                style={{flexShrink: 1, flexBasis: '55%'}}>
                <Text
                  testID="PR_graphcontainer_heading_text"
                  style={{fontSize: 12, fontFamily: 'Poppins-Medium'}}>
                  Summarizes the top ports open in the target infrastructure
                  based on the number of assets on which they are open
                </Text>
              </View>
              <View testID="PR_linegraph" style={{}}>
                <View style={{marginLeft: 120}}>
                  <LineGraph
                    heading="Assets"
                    graph_line={[
                      {
                        port: 5,
                        value: 1369,
                        text: 'CVE-2017-3167',
                        color: '#FF8930',
                      },
                      {
                        port: 4,
                        value: 1369,
                        text: 'CVE-2016-0736',
                        color: '#C61227',
                      },
                      {
                        port: 12,
                        value: 670,
                        text: 'CVE-2016-8740',
                        color: '#FFC736',
                      },
                      {
                        port: 10,
                        value: 620,
                        text: 'CVE-2015-3185',
                        color: '#C61227',
                      },
                      {
                        port: 2,
                        value: 400,
                        text: 'CVE-2016-4975',
                        color: '#FFC736',
                      },
                      {
                        port: 4,
                        value: 370,
                        text: 'CVE-2016-2161',
                        color: '#FF8930',
                      },
                      {
                        port: 8,
                        value: 340,
                        text: 'CVE-2017-9798',
                        color: '#FFC736',
                      },
                      {
                        port: 10,
                        value: 310,
                        text: 'CVE-2016-4975',
                        color: '#FF8930',
                      },
                      {
                        port: 15,
                        value: 280,
                        text: 'CVE-2017-3169',
                        color: '#FF8930',
                      },
                      {
                        port: 5,
                        value: 250,
                        text: 'CVE-2015-0228',
                        color: '#E35C6C',
                      },
                    ]}
                    maxValue={700}
                    // lineColor="#E35C6C"
                  />
                </View>
              </View>
            </View>
          </View>
          <View testID="PR_table" style={styles.PR_table}>
            {/* calling the resuseable table component  and the requride credentials  */}
            <ReportTable
              showLegend={true}
              heading="The patching cadence scoring component quantfies the operational security hygiene oF the target client by analyzing the unpatched CEs within the target asset infrastructure. The analysis is performmed gathering asset fingerprint details, determining applicable CVEs through virtual scaning and the time-topatch too those CVEs. "
              column_names={[
                'Control ID',
                'Asset',
                'RiskSense Risk Rating',
                'Description',
                'Score Impact',
              ]}
              widthArr={[
                widthPercentageToDP('11'),
                widthPercentageToDP('23'),
                widthPercentageToDP('16'),
                widthPercentageToDP('32'),
                widthPercentageToDP('12'),
              ]}
              column_data={current_data}
              // passing props required value for pagination just uncomment when connented to databaase
              RowPerPage={this.state.RowPerPage}
              totalRows={filtereddata.length}
              handlePageChange={this.handlePageChange}
              CurrentPage={this.state.CurrentPage}
            />
          </View>
          <View
            testID="PR_bottomcontainer"
            style={{
              marginTop: 20,
              marginBottom: 30,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              testID="PR_bottomcontainer_text"
              style={{flexShrink: 1, width: 500}}>
              <Text
                testID="PR_bottomcontainer_text_1"
                style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                Action items to improve the Organization Score -
                <Text
                  testID="PR_bottomcontainer_text_2"
                  style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                  Disable critical services like Redis, MySQL, and RDP
                </Text>
              </Text>
            </View>
            <View style={{marginLeft: 20}}>
              <CustomButton
                buttonTest="startButton"
                buttonBackgroundColor={false}
                textColor={true}
                hasShadow={true}
                navigation={this.props.navigation.navigate}
                handleButtonPress={this.handleNavigation}
                textStyle={mediumButton.buttonTextStyle}
                viewStyle={mediumButton.buttonViewStyle}
                handleButtonPress={this.navigation}>
                Get a Deeper Scan Report
              </CustomButton>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  PR_container: {
    paddingTop: 15,
    paddingHorizontal: 15,
    // height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    // fontWeight: '900',
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  PR_graphcontainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 20,
  },
});
