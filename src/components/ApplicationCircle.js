import React, {Component, useState, useEffect} from 'react';
import {Text, View, Animated, Easing} from 'react-native';
import Svg, {Circle, Text as SvgText, TSpan, G} from 'react-native-svg';

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

const Progress = props => {
  // start value of dash offset with circumference of circle
  const dash = new Animated.Value(props.circum);

  // animates dash offset from circumference of circle
  //  to the percentage of the value
  Animated.timing(dash, {
    toValue: props.circum * (1 - props.percent),
    easing: Easing.ease,
    duration: 1000,
  }).start();

  return <AnimatedCircle {...props} strokeDashoffset={dash} />;
};

export default class ApplicationCircle extends Component {
  state = {
    currentColor: '',
    percentageStroke: '',
    r: '',
    dash: '',
    circumference: '',
    circleStroke: '',
    progressStroke: '',
  };
  componentDidMount() {
    const {value1, value2, value3, maxValue, small} = this.props;
    const rad = small ? 17 : 50;

    this.setState({
      r: rad,
      percentageStroke1: value1 / maxValue,
      percentageStroke2: value2 / maxValue,
      percentageStroke3: value3 / maxValue,
    });
  }
  render() {
    const size1 = 70;
    const size2 = 60;
    const size3 = 50;
    const r1 = 70 - 8;
    const r2 = 60 - 8;
    const r3 = 50 - 8;
    const circumference1 = 2 * Math.PI * r1;
    const circumference2 = 2 * Math.PI * r2;
    const circumference3 = 2 * Math.PI * r3;

    const circleStroke = 6.4;
    const progressStroke = 8;
    const {percentageStroke1} = this.state;
    const {percentageStroke2} = this.state;
    const {percentageStroke3} = this.state;

    return (
      <View>
        <Svg style={{width: size1 * 2, height: size1 * 2}}>
          <Circle
            cx={size1}
            cy={size1}
            r={r1}
            stroke={this.props.small ? 'rgb(221,221,221)' : 'rgb(243,245,248)'}
            strokeWidth={circleStroke}
            fill="none"
          />
          <Progress
            cx={size1}
            cy={size1}
            r={r1}
            stroke={'#9E0A1B'}
            strokeWidth={progressStroke}
            fill="none"
            strokeLinecap="round"
            transform={`rotate(-90,${size1},${size1})`}
            strokeDasharray={`${circumference1}`}
            percent={percentageStroke1}
            circum={circumference1}
          />
          <Circle
            cx={size2 + 10}
            cy={size2 + 10}
            r={r2}
            stroke={this.props.small ? 'rgb(221,221,221)' : 'rgb(243,245,248)'}
            strokeWidth={circleStroke}
            fill="none"
          />
          <Progress
            cx={size2 + 10}
            cy={size2 + 10}
            r={r2}
            stroke={'#C61227'}
            strokeWidth={progressStroke}
            fill="none"
            strokeLinecap="round"
            transform={`rotate(-90,${size2 + 10},${size2 + 10})`}
            strokeDasharray={`${circumference2}`}
            percent={percentageStroke2}
            circum={circumference2}
          />
          <Circle
            cx={size3 + 20}
            cy={size3 + 20}
            r={r3}
            stroke={this.props.small ? 'rgb(221,221,221)' : 'rgb(243,245,248)'}
            strokeWidth={circleStroke}
            fill="none"
          />
          <Progress
            cx={size3 + 20}
            cy={size3 + 20}
            r={r3}
            stroke={'#FF8930'}
            strokeWidth={progressStroke}
            fill="none"
            strokeLinecap="round"
            transform={`rotate(-90,${size3 + 20},${size3 + 20})`}
            strokeDasharray={`${circumference3}`}
            percent={percentageStroke3}
            circum={circumference3}
          />
          <G x={size1} y={size1}>
            <SvgText textAnchor="middle">
              <TSpan
                x={0}
                fill="rgb(43,43,43)"
                fontSize={18}
                fontFamily="Poppins-SemiBold">
                193
              </TSpan>
              <TSpan
                x={0}
                y={16}
                fill="rgb(86,86,86)"
                fontSize={12}
                fontFamily="Poppins-Regular">
                total
              </TSpan>
            </SvgText>
          </G>
        </Svg>
      </View>
    );
  }
}
