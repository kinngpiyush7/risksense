import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import HighlightBox from '../components/HighlightBox';

export default class ReconnaissanceOverview extends Component {
  render() {
    return (
      <View style={styles.cardContainer}>
        <View testID="reconnaissanceOverviewCard" style={styles.cardInterior}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation('reconnaissanceOverview');
            }}>
            <Text testID="reconnaissanceOverview" style={styles.title}>
              Reconnaissance Overview
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginBottom: 5,
            }}>
            <View
              testID="vennDiagram"
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
              }}>
              <View style={styles.firstCircle}>
                <View style={{alignItems: 'center', alignSelf: 'center'}}>
                  <Text
                    style={{
                      color: '#565656',
                      fontSize: 15,
                      fontFamily: 'Poppins-SemiBold',
                    }}>
                    87
                  </Text>
                  <Text
                    testID=""
                    style={{
                      color: '#565656',
                      fontSize: 14,
                      fontFamily: 'Poppins-Regular',
                    }}>
                    IP
                  </Text>
                </View>
                <View
                  style={{
                    position: 'absolute',
                    right: 5,
                  }}>
                  <Text
                    style={{
                      fontSize: 13,
                      fontFamily: 'Poppins-Medium',
                    }}>
                    166
                  </Text>
                </View>
              </View>
              <View style={styles.secondCircle}>
                <Text
                  style={{
                    color: '#fff',
                    fontSize: 15,
                    fontFamily: 'Poppins-SemiBold',
                  }}>
                  42
                </Text>
                <Text
                  testID=""
                  style={{
                    color: '#fff',
                    fontSize: 14,
                    fontFamily: 'Poppins-Regular',
                  }}>
                  Domain
                </Text>
              </View>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation('reconnaissanceOverview');
            }}>
            <View testID="resultOfDomainCard" style={styles.domainCard}>
              <Text testID="resultOfDomain" style={styles.domain}>
                Result of Domain -{' '}
              </Text>
              <Text testID="resultOfDomainValue" style={styles.domainName}>
                www.merck.com
              </Text>
            </View>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <View>
              <Text testID="ipWithinDomain" style={styles.leftValues}>
                IP within Domain
              </Text>
              <Text testID="domainsWithoutIp" style={styles.leftValues}>
                Domains without IP
              </Text>
              <Text testID="subDomains" style={styles.leftValues}>
                Sub Domains
              </Text>
              <Text testID="fraudulentDomains" style={styles.leftValues}>
                Fraudulent Domains
              </Text>
            </View>
            <View style={{alignItems: 'center', width: 65}}>
              <Text testID="ipWithinDomainValue" style={styles.rightValues}>
                87
              </Text>
              <Text testID="domainsWithoutIpValue" style={styles.rightValues}>
                19
              </Text>
              <Text testID="subDomainsValue" style={styles.rightValues}>
                23
              </Text>
              <Text
                testID="fraudulentDomainsValue"
                style={{
                  fontSize: 14,
                  marginBottom: 10,
                  color: '#E35C6C',
                  fontWeight: 'bold',
                }}>
                7
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <Text
              testID="totalAssets"
              style={{fontSize: 14, fontFamily: 'Poppins-SemiBold'}}>
              Total Assets
            </Text>
            <HighlightBox testID="totalAssetsValue" boxColor={true}>
              166
            </HighlightBox>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardInterior: {
    borderColor: '#ededed',
    borderWidth: 2,
    borderRadius: 4,
    padding: 12,
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    color: 'rgb(43,43,43)',
    fontSize: 16,
    marginBottom: 20,
  },
  firstCircle: {
    backgroundColor: 'rgba(217, 217, 217,0.5)',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    width: 110,
    height: 110,
    borderRadius: 110 / 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    left: 17,
    zIndex: 2,
  },
  secondCircle: {
    backgroundColor: '#00B3C4',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,

    elevation: 4,
    width: 125,
    height: 125,
    borderRadius: 125 / 2,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'relative',
    right: 17,
  },
  domainCard: {
    flexDirection: 'row',
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#d4d4d4',
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  domain: {
    color: 'rgb(86,86,86)',
    fontSize: 14,
    fontFamily: 'Poppins-Medium',
  },
  domainName: {
    color: 'rgb(43,43,43)',
    fontSize: 14,
    fontFamily: 'Poppins-SemiBold',
  },
  leftValues: {
    fontSize: 14,
    marginBottom: 6,
    marginLeft: 2,
    fontFamily: 'Poppins-Medium',
  },
  rightValues: {
    fontSize: 14,
    marginBottom: 6,
    color: '#00B3C4',
    fontFamily: 'Poppins-SemiBold',
  },
});
