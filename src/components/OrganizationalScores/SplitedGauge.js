import React from 'react';
import Svg, {
  Circle,
  Text as SvgText,
  G,
  ClipPath,
  Rect,
  Image,
} from 'react-native-svg';

const r = 84;
const size = 103;

const SplitedGauge = () => {
  return (
    <Svg style={{width: size * 2 + 30, height: size + 60}}>
      {/* clippath to contain the gauge */}
      <ClipPath id="semi-circle">
        <Rect x="0" y="0" width={size * 2} height={r + 28} />
      </ClipPath>

      {/* gauge meter axial text */}
      <SvgText
        x={size + 15}
        y={14}
        textAnchor="middle"
        fontFamily="Poppins-Regular"
        color="rgb(86,86,86)"
        fontSize="11">
        600
      </SvgText>
      <SvgText
        x={size + 76}
        y={28}
        textAnchor="middle"
        fontFamily="Poppins-Regular"
        color="rgb(86,86,86)"
        fontSize="11">
        650
      </SvgText>
      <SvgText
        x={size + 112}
        y={62}
        textAnchor="middle"
        fontFamily="Poppins-Bold"
        color="rgb(86,86,86)"
        fontWeight={800}
        fontSize="11">
        700
      </SvgText>
      <SvgText
        x={size + 124}
        y={104}
        textAnchor="middle"
        fontFamily="Poppins-Bold"
        color="rgb(86,86,86)"
        fontWeight={800}
        fontSize="11">
        750
      </SvgText>

      {/* gauge starts */}

      <G clipPath="url(#semi-circle)" x="15" y="12">
        {/* white background arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(255,255,255)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(165,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${2 * Math.PI * r * 0.42}`}
        />
        {/* grey stroke arc */}
        <Circle
          cx={size}
          cy={size}
          r={r - 12}
          stroke={'rgb(233,236,239)'}
          strokeWidth="4"
          fill="none"
          transform={`rotate(165,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${2 * Math.PI * r * 0.48}`}
        />
        {/* red arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(213,62,79)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(165,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${2 * Math.PI * r * 0.712}`}
        />
        {/* orange arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(255,137,48)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(-90,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${Math.PI * r * (1.45 + 0.55 / 1.45)}`}
        />
        {/* yellow arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(251,206,63)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(-57.5,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${Math.PI * r * (1.45 + 0.55 / 1.31)}`}
        />
        {/* green arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(121,184,79)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(-32.5,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${Math.PI * r * (1.45 + 0.55 / 1.31)}`}
        />
        {/* blue arc */}
        <Circle
          cx={size}
          cy={size}
          r={r}
          stroke={'rgb(0,179,196)'}
          strokeWidth="20"
          fill="none"
          transform={`rotate(-8,${size},${size})`}
          strokeDasharray={`${2 * Math.PI * r}`}
          strokeDashoffset={`${Math.PI * r * (1.45 + 0.55 / 1.28)}`}
        />
      </G>
      <SvgText
        x={size + 15}
        y={size + 34}
        textAnchor="middle"
        fontFamily="Poppins-SemiBold"
        fontSize="16"
        fontWeight={800}
        letterSpacing="1.2"
        fill="rgb(78,133,42)">
        Great
      </SvgText>
      <G x={size + 4} y={size + 6}>
        <SvgText
          x={0}
          y={0}
          textAnchor="middle"
          fontFamily="RobotoCondensed-Bold"
          fill="rgb(43,43,43)"
          fontSize="30">
          +689
        </SvgText>
        <G x={46} y={-10}>
          <Circle cx={0} cy={0} r={12} fill="rgb(121,184,79)" />
          <Image
            x={-7}
            y={-32}
            width={14}
            href={require('../../assets/images/arrow-up.png')}
          />
        </G>
      </G>

      {/* gauge bottom text */}
      <SvgText
        x={34}
        y={size + 36}
        textAnchor="middle"
        fontFamily="Poppins-Regular"
        fill="rgb(86,86,86)"
        fontSize="11">
        300
      </SvgText>
      <SvgText
        x={size * 2 - 4}
        y={size + 36}
        textAnchor="middle"
        fontFamily="Poppins-Regular"
        fill="rgb(86,86,86)"
        fontSize="11">
        800
      </SvgText>
      <SvgText
        x={34}
        y={size + 52}
        textAnchor="middle"
        fontFamily="Poppins-SemiBold"
        fill="rgb(227,92,108)"
        fontWeight={800}
        fontSize="11">
        High Risk
      </SvgText>
      <SvgText
        x={size * 2 - 4}
        y={size + 52}
        textAnchor="middle"
        fontFamily="Poppins-SemiBold"
        fill="rgb(121,184,79)"
        fontWeight={800}
        fontSize="11">
        Low Risk
      </SvgText>
    </Svg>
  );
};

export default SplitedGauge;
