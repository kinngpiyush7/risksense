import React, {Component} from 'react';
import {View, TextInput, StyleSheet, Animated} from 'react-native';

export default class FloatingLabelInput extends Component {
  // Using the state for Floating Label - Eli
  state = {
    isFocused: false,
  };

  // Initializing the animation via onFocus - Eli
  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(0);
  }

  // Changing the value of the label onFocus / onPress of Floating Label Input - Eli
  handleFocus = () => this.setState({isFocused: true});
  // Disabling the styles which were enabled onFocus - Eli
  handleBlur = value => (value ? null : this.setState({isFocused: false}));

  // Enabling animation onFocus and passing the value on which the animation should happen along with the duration - Eli
  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: this.state.isFocused ? 1 : 0,
      duration: 300,
    }).start();
  }

  render() {
    // Using certain constants which will be used to render - Eli
    const {label, ...props} = this.props;
    const {isFocused} = this.state;
    // Using various styles for the label input - Eli
    const labelStyle = {
      position: 'relative',
      left: 0,
      // Using styles for Animation Focus - Eli
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [30, 10],
      }),
      // Using styles for Animation Focus - Eli
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [16, 14],
      }),
      fontFamily: 'Poppins-Medium',
      // Using styles for Animation Focus - Eli
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: ['rgb(86,86,86)', 'rgb(155,155,155)'],
      }),
      // Using styles for Animation Focus - Eli
      marginBottom: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [4, 12],
      }),
    };
    return (
      <View style={{paddingTop: 16}}>
        <Animated.Text style={labelStyle} testID={props.inputTest}>
          {label}
        </Animated.Text>
        {/* TextInput styles via props or custom along with onFocus and onBlur functionality - Eli  */}
        <TextInput
          {...props}
          testID={props.inputTest}
          onFocus={this.handleFocus}
          // Applying certain styles for the TextInput - Eli
          style={[
            styles.textInputField,
            isFocused ? {color: 'rgb(86,86,86)'} : null,
          ]}
          onBlur={() => this.handleBlur(this.props.value)}></TextInput>
      </View>
    );
  }
}

// The default styles applied on the button if the properties are not passed - Eli
const styles = StyleSheet.create({
  textInputField: {
    paddingBottom: 12,
    fontSize: 16,
    color: 'transparent',
    borderBottomWidth: 1,
    borderBottomColor: '#909090',
    fontFamily: 'Poppins-Medium',
  },
});
