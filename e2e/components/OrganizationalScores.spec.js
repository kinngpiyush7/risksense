describe('checks for the Organizational Scores flow', () => {
  beforeEach(async () => {
    device.reloadReactNative();
  });

  // test for organizational scores overview card
  it('checks for all the list item present in organizational score overview card', async () => {
    // test for presence of Organizational Scores Card heading
    await expect(element(by.id('organizationalCardHeading'))).toBeVisible();
    await expect(element(by.id('organizationalCardHeading'))).toHaveText(
      'Organizational xRS3 Scores',
    );

    // test for presence of IP Reputation list item
    await expect(element(by.id('ipReputationTitle'))).toBeVisible();
    await expect(element(by.id('ipReputationTitle'))).toHaveText(
      'IP Reputation',
    );

    await expect(element(by.id('ipReputationDescription'))).toBeVisible();
    await expect(element(by.id('ipReputationDescription'))).toHaveText(
      'Identifying the IP address which sending unwanted requests',
    );

    await expect(element(by.id('ipReputationIssue'))).toBeVisible();
    await expect(element(by.id('ipReputationIssue'))).toHaveText('0 Issue');

    // test for presence of Application Security list item
    await expect(element(by.id('appSecListTitle'))).toBeVisible();
    await expect(element(by.id('appSecListTitle'))).toHaveText(
      'Application Security',
    );

    await expect(element(by.id('appSecListDescription'))).toBeVisible();
    await expect(element(by.id('appSecListDescription'))).toHaveText(
      'Prevent from service attacks, cyberattacks and data breaches or data theft situations',
    );

    await expect(element(by.id('appSecListIssue'))).toBeVisible();
    await expect(element(by.id('appSecListIssue'))).toHaveText('13 Issue');

    // test for presence of Patching Cadence list item
    await expect(element(by.id('patchCadListTitle'))).toBeVisible();
    await expect(element(by.id('patchCadListTitle'))).toHaveText(
      'Patching Cadence',
    );

    await expect(element(by.id('patchCadListDescription'))).toBeVisible();
    await expect(element(by.id('patchCadListDescription'))).toHaveText(
      'Determining no. of vulnerabilities & how many critical vulnerabilities have yet to be provided',
    );

    await expect(element(by.id('patchCadListIssue'))).toBeVisible();
    await expect(element(by.id('patchCadListIssue'))).toHaveText('9 Issue');

    // test for presence of Network Security list item
    await expect(element(by.id('netSecListTitle'))).toBeVisible();
    await expect(element(by.id('netSecListTitle'))).toHaveText(
      'Network Security',
    );

    await expect(element(by.id('netSecListDescription'))).toBeVisible();
    await expect(element(by.id('netSecListDescription'))).toHaveText(
      'An integration of multiple layers of defenses in the network and at the network',
    );

    await expect(element(by.id('netSecListIssue'))).toBeVisible();
    await expect(element(by.id('netSecListIssue'))).toHaveText('10 Issue');

    // test for presence of Email Security list item
    await expect(element(by.id('emailSecListTitle'))).toBeVisible();
    await expect(element(by.id('emailSecListTitle'))).toHaveText(
      'Email Security',
    );

    await expect(element(by.id('emailSecListDescription'))).toBeVisible();
    await expect(element(by.id('emailSecListDescription'))).toHaveText(
      'Refers collective measures used to secure the access & content of an email account',
    );

    await expect(element(by.id('emailSecListIssue'))).toBeVisible();
    await expect(element(by.id('emailSecListIssue'))).toHaveText('21 Issue');

    // test for presence of DNS Security list item
    await expect(element(by.id('dnsSecListTitle'))).toBeVisible();
    await expect(element(by.id('dnsSecListTitle'))).toHaveText('DNS Security');

    await expect(element(by.id('dnsSecListDescription'))).toBeVisible();
    await expect(element(by.id('dnsSecListDescription'))).toHaveText(
      'Used to protect information on the Domain Name System',
    );

    await expect(element(by.id('dnsSecListIssue'))).toBeVisible();
    await expect(element(by.id('dnsSecListIssue'))).toHaveText('6 Issue');
  });

  // // test for application security card.
  // it('checks for the flow of the organizational list application security', async () => {
  //   // checks for the application security text to be visible and checks for it's text/
  //   await expect(element(by.id('appSecCardText'))).toBeVisible();
  //   await expect(element(by.id('appSecCardText'))).toHaveText(
  //     'Application Security',
  //   );

  //   // checks for the close icon
  //   await expect(element(by.id('closeIcon'))).toBeVisible();
  //   await expect(element(by.id('closeIcon'))).toHaveText('X');

  //   // checks for the header text button of the card
  //   await expect(element(by.id('appSecCardHeaderText'))).toBeVisible();
  //   await expect(element(by.id('appSecCardHeaderText'))).toHaveText(
  //     'High Risk Vulnerabilities',
  //   );

  //   // checks for the button
  //   await expect(element(by.id('appSecDetailReportButton'))).toBeVisible();
  //   await expect(element(by.id('appSecDetailReportButton'))).toHaveText(
  //     'Detail Report',
  //   );
  //   await element(by.id('appSecDetailReportButton')).tap();
  // });

  // //  test for pathcing cadence list
  // it('checks for the flow of the organizational list patching cadence', async () => {
  //   // checks for the patching cadence text to be visible and checks for it's text/
  //   await expect(element(by.id('patchCadCardText'))).toBeVisible();
  //   await expect(element(by.id('patchCadCardText'))).toHaveText(
  //     'Patching Cadence',
  //   );

  //   // checks for the close icon
  //   await expect(element(by.id('closeIcon'))).toBeVisible();
  //   await expect(element(by.id('closeIcon'))).toHaveText('X');

  //   // checks for the header text button of the card
  //   await expect(element(by.id('patchCadCardHeaderText'))).toBeVisible();
  //   await expect(element(by.id('patchCadCardHeaderText'))).toHaveText(
  //     'Need to Patch',
  //   );

  //   // checks for the description text button of the card
  //   await expect(element(by.id('patchCadDescription'))).toBeVisible();
  //   await expect(element(by.id('patchCadDescription'))).toHaveText(
  //     'The analysis is performed by passively gathering asset fingerprint details',
  //   );

  //   // checks for the button
  //   await expect(element(by.id('patchCadDetailReportButton'))).toBeVisible();
  //   await expect(element(by.id('patchCadDetailReportButton'))).toHaveText(
  //     'Detail Report',
  //   );
  //   await element(by.id('patchCadDetailReportButton')).tap();
  // });

  // // test for network security card
  // it('checks for the flow of the organizational list network security', async () => {
  //   // checks for the network security text to be visible and checks for it's text/
  //   await expect(element(by.id('netSecCardText'))).toBeVisible();
  //   await expect(element(by.id('netSecCardText'))).toHaveText(
  //     'Network Security',
  //   );

  //   // checks for the close icon
  //   await expect(element(by.id('closeIcon'))).toBeVisible();
  //   await expect(element(by.id('closeIcon'))).toHaveText('X');

  //   // checks for the header text button of the card
  //   await expect(element(by.id('netSecCardHeaderText'))).toBeVisible();
  //   await expect(element(by.id('netSecCardHeaderText'))).toHaveText(
  //     'High Risk Assets',
  //   );

  //   // checks for the description text button of the card
  //   await expect(element(by.id('netSecDescription'))).toBeVisible();
  //   await expect(element(by.id('netSecDescription'))).toHaveText('Open Port');

  //   // checks for the button
  //   await expect(element(by.id('netSecDetailReportButton'))).toBeVisible();
  //   await expect(element(by.id('netSecDetailReportButton'))).toHaveText(
  //     'Detail Report',
  //   );
  //   await element(by.id('netSecDetailReportButton')).tap();
  // });

  // // test for email security card
  // it('checks for the flow of the organizational list email security', async () => {
  //   // checks for the email security text to be visible and checks for it's text/
  //   await expect(element(by.id('emailSecCardText'))).toBeVisible();
  //   await expect(element(by.id('emailSecCardText'))).toHaveText(
  //     'Email Security',
  //   );

  //   // checks for the close icon
  //   await expect(element(by.id('closeIcon'))).toBeVisible();
  //   await expect(element(by.id('closeIcon'))).toHaveText('X');

  //   // checks for the header text button of the card
  //   await expect(element(by.id('emailSecCardHeaderText'))).toBeVisible();
  //   await expect(element(by.id('emailSecCardHeaderText'))).toHaveText(
  //     'Top Findings',
  //   );

  //   // checks for the description text button of the card
  //   await expect(element(by.id('emailSecCWE'))).toBeVisible();
  //   await expect(element(by.id('emailSecCWE'))).toHaveText(
  //     'Common Weakness Enumeration',
  //   );
  //   await expect(element(by.id('emailSecSMTP'))).toBeVisible();
  //   await expect(element(by.id('emailSecSMTP'))).toHaveText(
  //     'Sender Policy Framework',
  //   );

  //   // checks for the button
  //   await expect(element(by.id('emailSecDetailReportButton'))).toBeVisible();
  //   await expect(element(by.id('emailSecDetailReportButton'))).toHaveText(
  //     'Detail Report',
  //   );
  //   await element(by.id('emailSecDetailReportButton')).tap();
  // });

  // // test for dns security card
  // it('checks for the flow of the organizational list dns security', async () => {
  //   // checks for the dns security text to be visible and checks for it's text/
  //   await expect(element(by.id('dnsSecCardText'))).toBeVisible();
  //   await expect(element(by.id('dnsSecCardText'))).toHaveText('DNS Security');

  //   // checks for the close icon
  //   await expect(element(by.id('closeIcon'))).toBeVisible();
  //   await expect(element(by.id('closeIcon'))).toHaveText('X');

  //   // checks for the header text button of the card
  //   await expect(element(by.id('dnsSecCardHeaderText'))).toBeVisible();
  //   await expect(element(by.id('dnsSecCardHeaderText'))).toHaveText(
  //     'Top Findings',
  //   );

  //   // checks for the description text button of the card
  //   await expect(element(by.id('dnsSecCWE'))).toBeVisible();
  //   await expect(element(by.id('dnsSecCWE'))).toHaveText(
  //     'Common Weakness Enumeration',
  //   );
  //   await expect(element(by.id('dnsSecDNS'))).toBeVisible();
  //   await expect(element(by.id('dnsSecDNS'))).toHaveText('Domain Name System');

  //   // checks for the button
  //   await expect(element(by.id('dnsSecDetailReportButton'))).toBeVisible();
  //   await expect(element(by.id('dnsSecDetailReportButton'))).toHaveText(
  //     'Detail Report',
  //   );
  //   await element(by.id('dnsSecDetailReportButton')).tap();
  // });
});
