import React from 'react';
import {View, Text, Animated} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
const LineGraphView = props => {
  const fadeAnim = new Animated.Value(0); // Initial value for opacity: 0

  React.useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: props.barwidth,
      duration: 1000,
    }).start();
  }, []);

  return (
    <Animated.View // Special animatable View
      style={{
        ...props.style,
        width: fadeAnim,
      }}>
      {props.children}
    </Animated.View>
  );
};
const LineGraph = props => {
  return (
    <View>
      <Text
        style={{
          marginBottom: 4,
          fontSize: 9.6,
          fontFamily: 'Poppins-SemiBold',
        }}>
        {props.heading}
      </Text>
      <View style={{flexDirection: 'row'}}>
        <View>
          <FlatList
            data={props.graph_line}
            renderItem={({item}) => (
              <View
                style={{
                  height: 10,
                  marginBottom: 6.1,
                  marginRight: 10,
                }}>
                <Text style={{fontSize: 8.8, fontFamily: 'Poppins-Medium'}}>
                  {item.text}
                </Text>
              </View>
            )}
          />
        </View>
        <View style={{marginTop: 2}}>
          <FlatList
            data={props.graph_line}
            renderItem={({item}) => (
              <View style={{flexDirection: 'row'}}>
                <LineGraphView
                  barwidth={(item.value / props.maxValue) * 100}
                  style={{
                    height: 8,
                    backgroundColor: item.color,
                    marginBottom: 6,
                    marginTop: 2,
                    borderTopRightRadius: 5,
                    borderBottomRightRadius: 5,
                    marginRight: 4,
                  }}></LineGraphView>
                <Text
                  style={{
                    color: '#939393',
                    fontSize: 8.8,
                    fontFamily: 'Poppins-Regular',
                    marginLeft: 3,
                  }}>
                  {item.port}
                </Text>
              </View>
            )}
          />
        </View>
      </View>
    </View>
  );
};
export default LineGraph;
