describe('Test for Email Report Screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('The Thank You Screen should be tested along with included Reuseable Components', async () => {
    const fname = 'Eliyahoo';
    const lname = 'Awaskar';
    const c_email = 'headstrait@gmail.com';
    const c_url = 'headstrait.com';
    await waitFor(element(by.id('demo')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('startButton')).tap();
    await element(by.id('input_name')).tap();
    await waitFor(element(by.id('input_name')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_name')).typeText(fname);
    await element(by.id('input_lname')).tap();
    await waitFor(element(by.id('input_lname')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_lname')).typeText(lname);
    await element(by.id('input_email')).tap();
    await waitFor(element(by.id('input_email')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_email')).typeText(c_email);
    await device.pressBack();
    await waitFor(element(by.id('input_url')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_url')).tap();
    await waitFor(element(by.id('input_url')))
      .toBeVisible()
      .withTimeout(30000);
    await device.pressBack();
    await waitFor(element(by.id('requestButton')))
      .toBeVisible()
      .withTimeout(30000);
    await element(by.id('requestButton')).tap();
    // await waitFor(element(by.id('demo2')))
    //   .toBeVisible()
    //   .withTimeout(10000);
    // await expect(element(by.id('demo2'))).toBeVisible();
    // await waitFor(element(by.id('thankyou_screen')))
    //   .toBeVisible()
    //   .withTimeout(10000);
    // await expect(element(by.id('thankyou_button'))).toBeVisible();
    // await element(by.id('thankyou_button')).tap();
    // await expect(element(by.id('NR_backarrow_button'))).toBeVisible();
    // await element(by.id('NR_backarrow_button')).tap();
    // await expect(element(by.id('NR_backarrow'))).toBeVisible();
    // await expect(element(by.id('NR_heading'))).toBeVisible();
    // await expect(element(by.id('NR_heading'))).toHaveText(
    //   'Network Security Report',
    // );
    // await expect(element(by.id('NR_sub_heading'))).toBeVisible();
    // await expect(element(by.id('NR_sub_heading'))).toHaveText(
    //   'An integration of multiple layers of defenses in the network and at the network',
    // );

    // await expect(element(by.id('NR_graphcontainer_heading_text'))).toHaveText(
    //   'Summarizes the top ports open in the target infrastructure based on the number of assets on which they are open',
    // );
    // await expect(element(by.id('NR_bottomcontainer_text_1'))).toBeVisible();
    // await expect(element(by.id('NR_bottomcontainer_text_1'))).toHaveText(
    //   'Action items to improve the Organization Score -Disable critical services like Redis, MySQL, and RDP',
    // );
    // await expect(element(by.id('NR_bottomcontainer_text_2'))).toBeVisible();
    // await expect(element(by.id('NR_bottomcontainer_text_2'))).toHaveText(
    //   '- Disable critical services like Redis, MySQL, and RDP',
    // );
  });
});
