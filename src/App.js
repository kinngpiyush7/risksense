import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import GlobalRanking from './components/GlobalRanking';
import SubscriptionPlan from './screens/SubscriptionPlan';
// import Orientation from 'react-native-orientation';
import StartScreen from './screens/StartScreen';
import RequestScreen from './screens/RequestScreen';
import SplashScreen from './screens/SplashScreen';
import ThankYouScreen from './screens/ThankYouScreen';
import DashBoard from './screens/DashBoard';
import PatchReport from './screens/PatchReport';
import DNSReport from './screens/DNSReport';
import ApplicationReport from './screens/ApplicationReport';
import CheckNewDomain from './screens/CheckDomainScreen';

import NetworkReport from './screens/NetworkReport';
import EmailReport from './screens/EmailReport';
import ReconnaissanceOverview from './screens/ReconnaissanceOverview';

import CyberRiskTrend from './components/CyberRiskTrend';
// provider and store for redux
import {Provider} from 'react-redux';
import store from './store';

const AppNavigator = createStackNavigator(
  {
    SubscriptionPlan: SubscriptionPlan,
    ApplicationReport: ApplicationReport,
    NetworkReport: NetworkReport,
    startScreen: StartScreen,
    requestScreen: RequestScreen,
    splashScreen: SplashScreen,
    thankYouScreen: ThankYouScreen,
    DashBoard: DashBoard,
    PatchReport: PatchReport,
    checkNewDomain: CheckNewDomain,
    emailReport: EmailReport,
    DNSReport: DNSReport,
    reconnaissanceOverview: ReconnaissanceOverview,
    CyberRiskTrend: CyberRiskTrend,
  },
  {
    initialRouteName: 'startScreen',
  },
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends React.Component {
  componentDidMount() {
    // Orientation.lockToLandscape();
  }
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
