import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import ReportTable from '../components/ReportTable';
import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import CustomButton from '../components/CustomButton';
import Tag from '../components/OrganizationalScores/Tag';
import email_image from '../assets/images/emailSecurity/srsIconsEmailSecurity.png';
import {mediumButton} from '../assets/styles/Buttons';

import {SafeAreaView} from 'react-navigation';
import {widthPercentageToDP} from '../components/Responsive';
import DonutGraph from '../components/DonutGraph';

export default class EmailReport extends Component {
  state = {
    CurrentPage: 1,
    RowPerPage: 10,
  };

  static navigationOptions = {
    header: null,
  };

  handlePageChange = pageNumber => {
    this.setState({CurrentPage: pageNumber});
  };

  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };
  render() {
    const data = [
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'CWS-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8b34',
              borderColor: '#ff8b34',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'DNS Sec. KEY rotation period not within recommended range',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'ALT2.ASPMX.I.GOOGLE.com PORT 993',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        ,
      ],
      [
        <Tag
          tag={'CWE-10'}
          customStyle={{
            marginRight: 4,
            width: 64,
            fontSize: 15,
            backgroundColor: '#c61227',
            borderColor: '#c61227',
          }}
          customText={{
            fontSize: 11,
          }}
        />,
        'ASPMX.I.GOOGLE.com PORT 143',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'MX servers port 25 connection successfully',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8b34',
              borderColor: '#ff8b34',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'CWE-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#9e0a1b',
              borderColor: '#9e0a1b',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'MX PTR record found. Reverse DNS Test success',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#9e0a1b',
              borderColor: '#9e0a1b',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'Reverse DNS(PTR Record) is a valid HostName',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <Tag
          tag={'SMTP-213'}
          customStyle={{
            marginRight: 4,
            width: 64,
            fontSize: 15,
            backgroundColor: '#f2b924',
            borderColor: '#f2b924',
          }}
          customText={{
            fontSize: 11,
          }}
        />,
        'DKIM Record Not Found',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <Tag
          tag={'CWE-10'}
          customStyle={{
            marginRight: 4,
            width: 64,
            fontSize: 15,
            backgroundColor: '#ff8930',
            borderColor: '#ff8930',
          }}
          customText={{
            fontSize: 11,
          }}
        />,
        '173.194.213.26<<<>>>vr-in-f26.1e100.net',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'ALT2.ASPMX.I.GOOGLE.com PORT 993',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        ,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8930',
              borderColor: '#ff8930',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'CWE-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#9e0a1b',
              borderColor: '#9e0a1b',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'MX PTR record found. Reverse DNS Test success',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'ALT2.ASPMX.I.GOOGLE.com PORT 993',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'CWS-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8930',
              borderColor: '#ff8930',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'DNS Sec. KEY rotation period not within recommended range',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'MX servers port 25 connection successfully',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#9e0a1b',
              borderColor: '#9e0a1b',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'Reverse DNS(PTR Record) is a valid HostName',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8930',
              borderColor: '#ff8930',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'SMTP-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#00b3c4',
              borderColor: '#00b3c4',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'ALT2.ASPMX.I.GOOGLE.com PORT 993',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <Tag
            tag={'CWE-10'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#f2b924',
              borderColor: '#f2b924',
            }}
            customText={{
              fontSize: 11,
            }}
          />
          <Tag
            tag={'CWE-213'}
            customStyle={{
              marginRight: 4,
              // width: 64,
              fontSize: 15,
              backgroundColor: '#ff8930',
              borderColor: '#ff8930',
            }}
            customText={{
              fontSize: 11,
            }}
          />
        </View>,
        'MX PTR record found. Reverse DNS Test success',
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
      ],
    ];

    //will be use for pagination when we get data from back end
    let filteredData = data;
    // [Pagination] Fetching the first and last companies index
    // and slicing to get current companies
    let indexOfLastRow = this.state.CurrentPage * this.state.RowPerPage;
    let indexOfFirstRow = indexOfLastRow - this.state.RowPerPage;
    let current_data =
      filteredData !== null
        ? filteredData.slice(indexOfFirstRow, indexOfLastRow)
        : null;

    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="NS_container" style={styles.NS_container}>
            <View testID="NR_header" style={styles.header}>
              <TouchableOpacity
                testID="NR_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="NR_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="PR_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={email_image}></Image>
              <View testID="NR_header_texts" style={styles.header_texts}>
                <Text testID="NR_heading" style={styles.heading}>
                  Email Security Report
                </Text>
                <Text testID="NR_sub_heading" style={styles.sub_heading}>
                  Refers collective measures used to secure the access & content
                  of an email account
                </Text>
              </View>
            </View>

            <View testID="NR_graphcontainer" style={styles.NR_graphcontainer}>
              <View
                style={{
                  flexBasis: '25%',
                  paddingTop: 25,
                  paddingBottom: 25,
                }}>
                <DonutGraph
                  keys={[1, 2, 3, 4]}
                  values={[55, 7, 18, 20]}
                  colors={['#C61227', '#FF8930', '#79B84F', '#FFC736']}
                  customStyles={styles.pie_main}
                  outerRadius="100%"
                  innerRadius="70%"
                  text1="166"
                  text2="Total"
                />
              </View>
              <View
                style={{
                  flexBasis: '75%',
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <View
                  testID="NR_linegraph"
                  style={{
                    flexBasis: '45%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  {/* <PieChart */}

                  <View
                    style={{
                      flexBasis: '75%',
                    }}>
                    <Text style={styles.donut_info}>Dangerous File Types</Text>
                    <Text style={styles.donut_info}>Spam</Text>
                  </View>
                  <View
                    style={{
                      flexBasis: '15%',
                    }}>
                    <Text style={[styles.donut_info, {color: '#C61227'}]}>
                      87
                    </Text>
                    <Text style={[styles.donut_info, {color: '#FF8930'}]}>
                      19
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: 0.8,
                    backgroundColor: '#E0E0E0',
                    height: 90,
                    alignSelf: 'center',
                  }}
                />

                <View
                  style={{
                    flexBasis: '45%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignSelf: 'center',
                  }}>
                  <View
                    style={{
                      flexBasis: '75%',
                    }}>
                    <Text style={styles.donut_info}>Malware Attachments </Text>
                    <Text style={styles.donut_info}>Impersonation Attacks</Text>
                  </View>
                  <View
                    style={{
                      flexBasis: '15%',
                    }}>
                    <Text
                      style={[
                        styles.donut_info,
                        {
                          color: '#79B84F',
                        },
                      ]}>
                      23
                    </Text>
                    <Text
                      style={[
                        styles.donut_info,
                        {
                          color: '#FFC736',
                        },
                      ]}>
                      7
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View testID="NR_table" style={styles.NR_table}>
              {filteredData.length > 0 ? (
                <ReportTable
                  showLegend={true}
                  tableMargin={{marginHorizontal: 10}}
                  heading="TThe email security scoring component represents the security practices around the client's email infrastructure. The following components are tested and verified for best practices:"
                  column_names={['Control ID', 'Vulnerability', 'Description']}
                  widthArr={[
                    widthPercentageToDP('20'),
                    widthPercentageToDP('32'),
                    widthPercentageToDP('39'),
                  ]}
                  column_data={current_data}
                  // passing props required value for pagination just uncomment when connented to databaase
                  RowPerPage={this.state.RowPerPage}
                  totalRows={filteredData.length}
                  handlePageChange={this.handlePageChange}
                  CurrentPage={this.state.CurrentPage}
                />
              ) : (
                <View testID="no-result-found"> No Result Found</View>
              )}
            </View>
            <View
              testID="NR_bottomcontainer"
              style={{
                marginTop: 20,
                marginBottom: 30,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                testID="NR_bottomcontainer_text"
                style={{flexShrink: 1, width: 500}}>
                <Text
                  testID="NR_bottomcontainer_text_1"
                  style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                  Action items to improve the Organization Score -
                  <Text
                    testID="NR_bottomcontainer_text_2"
                    style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                    Disable critical services like Redis, MySQL, and RDP
                  </Text>
                </Text>
              </View>
              <View style={{marginLeft: 20}}>
                <CustomButton
                  buttonTest="startButton"
                  buttonBackgroundColor={false}
                  textColor={true}
                  hasShadow={true}
                  navigation={this.props.navigation.navigate}
                  handleButtonPress={this.handleNavigation}
                  textStyle={mediumButton.buttonTextStyle}
                  viewStyle={mediumButton.buttonViewStyle}
                  handleButtonPress={this.navigation}>
                  Get a Deeper Scan Report
                </CustomButton>
              </View>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  NS_container: {
    paddingTop: 15,
    paddingHorizontal: 10,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  donut_info: {
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
  },
  NR_graphcontainer: {
    flexDirection: 'row',
    width: '70%',
    marginLeft: 'auto',
  },
  pie_main: {
    height: 140,
    transform: [{rotateX: 3}, {rotateZ: -0.3}],
  },
  table: {
    backgroundColor: '#F7F7F7',
    // padding: 15,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#D2D2D2',
  },
  container: {marginTop: 20},
  head: {
    fontSize: 14,
    paddingLeft: 15,
    // fontFamily: 'Poppins-Medium',
  },
  headtext: {color: '#8C8C8C', fontFamily: 'Poppins-Medium', fontSize: 14},
  datatext: {fontFamily: 'Poppins-Regular', fontSize: 13},
  columndata: {
    borderBottomWidth: 1,
    borderBottomColor: '#E2E2E2',
    paddingVertical: 12,
  },
  centre: {
    paddingLeft: 15,
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
  header_style: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    padding: 10,
  },
  fraudulent_style: {
    borderLeftWidth: 3,
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
    borderLeftColor: '#E35C6C',
    paddingLeft: 15,
  },
  fraudulent_value: {
    color: '#C61227',
    paddingLeft: 15,
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
});
