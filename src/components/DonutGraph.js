import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import {PieChart} from 'react-native-svg-charts';

export default class DonutGraph extends Component {
  constructor(props) {
    super(props);
    // Using the label and value from various parts on the donut - Eli
    this.state = {
      selectedSlice: {
        label: 3,
        value: 0,
      },
      labelWidth: 0,
    };
  }

  render() {
    // Using constants using to render - Eli
    const {selectedSlice} = this.state;
    const {...props} = this.props;
    const {label} = selectedSlice;
    // The keys, Values and Colors are passed by the Component calling the Graph or default of these are used - Eli
    const keys = props.keys ? this.props.keys : [1, 2, 3, 4, 5];
    const values = props.values ? this.props.values : [25, 25, 25, 25, 25];
    const colors = props.colors
      ? this.props.colors
      : ['#160304', '#5adcf2', '#59d27d', '#984109', '#82213f'];
    // The Keys mapping is done here - Eli
    const data_use = keys.map((key, index) => {
      return {
        key,
        value: values[index],
        svg: {fill: colors[index]},
        // The radius is changed according to the key - Eli
        arc: {
          outerRadius: label === key ? 70 : 60,
          padAngle: label === 0,
        },
        // The Radius of different parts are changed according to the parts pressed - Eli
        onPress: () =>
          this.setState({selectedSlice: {label: key, value: values[index]}}),
      };
    });
    return (
      <>
        {/* Using the PieChart component from the included package and passing it certain props - Eli  */}
        {/* Passing the data which is to be mapped as a prop to PieChart component - Eli  */}
        {/* Increase in the InnerRadius helps the PieChart to resemble like a DonutGraph - Eli  */}
        <PieChart
          style={props.customStyles ? props.customStyles : null}
          outerRadius={props.outerRadius ? props.outerRadius : '100%'}
          innerRadius={props.innerRadius ? props.innerRadius : '50%'}
          data={data_use}
        />
        {/* Using the Text which will be shown in the centre of the DonutGraph - Eli  */}
        <Text style={styles.text_inside_1}>{props.text1}</Text>
        <Text style={styles.text_inside_2}>
          {'\n'}
          {props.text2}
        </Text>
      </>
    );
  }
}

// The default styles applied on the button if the properties are not passed - Eli
const styles = StyleSheet.create({
  text_inside_1: {
    position: 'absolute',
    textAlign: 'center',
    fontSize: 25,
    left: '40%',
    top: '50%',
    fontFamily: 'Poppins-SemiBold',
  },
  text_inside_2: {
    position: 'absolute',
    textAlign: 'center',
    color: '#4A4A4A',
    fontSize: 15,
    left: '40%',
    top: '56%',
    fontFamily: 'Poppins-Regular',
  },
});
