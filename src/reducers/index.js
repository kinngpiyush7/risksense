import {combineReducers} from 'redux';
import request from './RequestReducer';

export default combineReducers({
  requestReducer: request,
});
