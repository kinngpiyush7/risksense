import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  ImageBackground,
} from 'react-native';
import CustomDescriptionBox from '../components/CustomDescriptionBox';
import CustomButton from '../components/CustomButton';
import {largeButton} from '../assets/styles/Buttons';
import {heightPercentageToDP} from '../components/Responsive';

import {SafeAreaView} from 'react-navigation';

export default class StartScreen extends Component {
  static navigationOptions = {header: null};

  handleNavigation = () => {
    this.props.navigation.navigate('requestScreen');
  };

  render() {
    return (
      <SafeAreaView testID="demo">
        <ImageBackground
          source={require('../assets/images/LoadingScreen.png')}
          style={styles.backgroundImage}>
          <View style={styles.mainView}>
            <View style={styles.main}>
              <View style={styles.image_style}>
                <Image
                  testID="start_image"
                  style={styles.image}
                  source={require('../assets/images/risksense-logo/logo.png')}
                />
              </View>
              <View style={styles.sample}>
                <CustomDescriptionBox
                  headerTest="startId"
                  header="Learn, Manage, and Mitigate your Business Cyber Risks"
                  text1="RiskSense SRS provides a clear picture about the security
                        posture of your business ecosystem; an 'outside-in' view
                         of your enterprise security"
                  text2={
                    <Text>
                      Ready for a quick view of your{' '}
                      <Text style={{fontFamily: 'Poppins-Bold'}}>FREE</Text>{' '}
                      Cyber Risk Score Card?
                    </Text>
                  }
                />
              </View>
            </View>
            <View style={styles.main2}>
              <CustomButton
                buttonTest="startButton"
                buttonBackgroundColor={true}
                textColor={false}
                hasShadow={true}
                isFullWidth={false}
                viewStyle={[{paddingHorizontal: 10, paddingVertical: 16}]}
                textStyle={largeButton.buttonTextStyle}
                navigation={this.props.navigation.navigate}
                handleButtonPress={this.handleNavigation}>
                Let's Start
              </CustomButton>
            </View>
          </View>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  mainView: {
    padding: 15,
    flexDirection: 'row',
    height: Dimensions.get('window').height,
  },
  image_style: {
    marginLeft: 85,
    marginTop: 42,
    width: 211,
    height: 62,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  main: {
    flexBasis: '76%',
  },
  sample: {
    marginTop: heightPercentageToDP('15'),
  },
  main2: {
    alignSelf: 'flex-end',
    flexBasis: '18%',
    marginBottom: heightPercentageToDP('10'),
  },
  startHeight: {
    height: Dimensions.get('window').height / 3 + 40,
  },
});
