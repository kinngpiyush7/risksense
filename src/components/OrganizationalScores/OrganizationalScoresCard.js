import React, {Component} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';

import OrgScoreListItemTouchable, {OrgScoreListItem} from './OrgScoreListItem';
import SplitedGauge from './SplitedGauge';

const ipReputation = {
  title: 'IP Reputation',
  titleTestId: 'ipReputationTitle',
  description: 'Identifying the IP address which sending unwanted requests',
  descTestId: 'ipReputationDescription',
  issue: 0,
  issueTestId: 'ipReputationIssue',
  progress: 202,
};
const orgScoreData = [
  {
    title: 'Application Security',
    titleTestId: 'appSecListTitle',
    description:
      'Prevent from service attacks, cyberattacks and data breaches or data theft situations',
    descTestId: 'appSecListDescription',
    issue: 13,
    issueTestId: 'appSecListIssue',
    progress: 652,
  },
  {
    title: 'Patching Cadence',
    titleTestId: 'patchCadListTitle',
    description:
      'Determining no. of vulnerabilities & how many critical vulnerabilities have yet to be provided',
    descTestId: 'patchCadListDescription',
    issue: 9,
    issueTestId: 'patchCadListIssue',
    progress: 481,
  },
  {
    title: 'Network Security',
    titleTestId: 'netSecListTitle',
    description:
      'An integration of multiple layers of defenses in the network and at the network',
    descTestId: 'netSecListDescription',
    issue: 10,
    issueTestId: 'netSecListIssue',
    progress: 549,
  },
  {
    title: 'Email Security',
    titleTestId: 'emailSecListTitle',
    description:
      'Refers collective measures used to secure the access & content of an email account',
    descTestId: 'emailSecListDescription',
    issue: 21,
    issueTestId: 'emailSecListIssue',
    progress: 437,
  },
  {
    title: 'DNS Security',
    titleTestId: 'dnsSecListTitle',
    description: 'Used to protect information on the Domain Name System',
    descTestId: 'dnsSecListDescription',
    issue: 6,
    issueTestId: 'dnsSecListIssue',
    progress: 601,
  },
];
export default class OrganizationalScoresCard extends Component {
  render() {
    return (
      <View style={[styles.cardContainer]}>
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginLeft: 14,
            }}>
            <Text
              testID="organizationalCardHeading"
              style={{
                fontFamily: 'Poppins-SemiBold',
                color: 'rgb(43,43,43)',
                marginTop: 12,
                fontSize: 16,
              }}>
              Organizational xRS3 Scores
            </Text>
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 10,
            }}>
            <SplitedGauge />
          </View>

          <OrgScoreListItem
            listStyle={{marginTop: 20}}
            listItem={ipReputation}
            currentItem={this.props.currentItem}
          />

          <FlatList
            data={orgScoreData}
            renderItem={({item, index}) => (
              <OrgScoreListItemTouchable
                listItem={item}
                currentItem={this.props.currentItem}
                changeCard={screen => this.props.activeScreen(screen)}
              />
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: 'rgb(0,179,196)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 4,
    backgroundColor: 'rgb(246,254,255)',
    paddingBottom: 14,
    marginRight: 12,
    height: 690,
  },
});
