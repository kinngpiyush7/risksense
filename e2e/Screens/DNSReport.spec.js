describe('Test for DNS Report page', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('DNS Report page', async () => {
    await expect(element(by.id('DNS_backarrow_button'))).toBeVisible();
    await element(by.id('DNS_backarrow_button')).tap();
    await expect(element(by.id('DNS_backarrow'))).toBeVisible();
    await expect(element(by.id('DNS_reportimage'))).toBeVisible();
    await expect(element(by.id('DNS_heading'))).toBeVisible();
    await expect(element(by.id('DNS_heading'))).toHaveText('DNS Security');
    await expect(element(by.id('DNS_sub_heading'))).toBeVisible();
    await expect(element(by.id('DNS_sub_heading'))).toHaveText(
      'Used to rotect information on the Domain Name System',
    );
    await expect(element(by.id('DNS_graphcontainer'))).toBeVisible();
    await expect(element(by.id('DNS_table'))).toBeVisible();

    // await expect(element(by.id('DNS_bottomcontainer_text_1'))).toBeVisible();
    await expect(element(by.id('DNS_bottomcontainer_text_1'))).toHaveText(
      'Action items to improve the Organization Score -Disable critical services like Redis, MySQL, and RDP',
    );
    // await expect(element(by.id('DNS_bottomcontainer_text_2'))).toBeVisible();
    // await expect(element(by.id('DNS_bottomcontainer_text_2'))).toHaveText(
    //   '- Disable critical services like Redis, MySQL, and RDP',
    // );
  });
});
