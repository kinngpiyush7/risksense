describe('Test for Patch Report page', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Ptch Report page', async () => {
    await expect(element(by.id('PR_backarrow_button'))).toBeVisible();
    await element(by.id('PR_backarrow_button')).tap();
    await expect(element(by.id('PR_backarrow'))).toBeVisible();
    await expect(element(by.id('PR_heading'))).toBeVisible();
    await expect(element(by.id('PR_heading'))).toHaveText('Patch Cadence');
    await expect(element(by.id('PR_sub_heading'))).toBeVisible();
    await expect(element(by.id('PR_sub_heading'))).toHaveText(
      'Determining no. of vulnerabilities and how many critical vulnerabilities have yet to be patched',
    );

    await expect(element(by.id('PR_graphcontainer_heading_text'))).toHaveText(
      'Summarizes the top ports open in the target infrastructure based on the number of assets on which they are open',
    );
    // await expect(element(by.id('PR_bottomcontainer_text_1'))).toBeVisible();
    await expect(element(by.id('PR_bottomcontainer_text_1'))).toHaveText(
      'Action items to improve the Organization Score -Disable critical services like Redis, MySQL, and RDP',
    );
    // await expect(element(by.id('PR_bottomcontainer_text_2'))).toBeVisible();
    // await expect(element(by.id('PR_bottomcontainer_text_2'))).toHaveText(
    //   '- Disable critical services like Redis, MySQL, and RDP',
    // );
  });
});
