import React from 'react';
import {View, StyleSheet, Platform} from 'react-native';

const StatusBar = props => {
  return <View style={[styles.statusBarBackground, props.style || {}]}></View>;
};

const styles = StyleSheet.create({
  statusBarBackground: {
    height: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: 'white',
  },
});

export default StatusBar;
