import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

const colorsArray = [
  'rgb(121,184,79)',
  'rgb(251,206,63)',
  'rgb(255,137,48)',
  'rgb(213,62,79)',
  'rgb(158,10,27)',
];
export default class ScoreCards extends Component {
  state = {
    currentColor: '',
  };
  componentDidMount() {
    if (this.props.dynamicColor) {
      if (this.props.score === 0) this.setState({currentColor: colorsArray[0]});
      else if (this.props.score >= 1 && this.props.score <= 10)
        this.setState({currentColor: colorsArray[1]});
      else if (this.props.score >= 11 && this.props.score <= 20)
        this.setState({currentColor: colorsArray[2]});
      else if (this.props.score >= 21 && this.props.score <= 40)
        this.setState({currentColor: colorsArray[3]});
      else if (this.props.score >= 41)
        this.setState({currentColor: colorsArray[4]});
    } else {
      this.setState({currentColor: 'rgb(234,234,234)'});
    }
  }
  render() {
    return (
      <View style={styles.cardsContainer}>
        <View
          style={[
            styles.scoreContainer,
            {backgroundColor: this.state.currentColor},
          ]}>
          <Text
            testID={this.props.scoreCardScoreId}
            style={{
              color: this.props.dynamicColor ? '#ffffff' : 'rgb(43,43,43)',
              textAlign: 'center',
              fontSize: 12,
              fontFamily: this.props.dynamicColor
                ? 'Poppins-SemiBold'
                : 'Poppins-Regular',
            }}>
            {this.props.score}
          </Text>
        </View>
        <View style={[styles.titleContainer]}>
          <Text
            testID={this.props.scoreCardTitleID}
            style={{
              fontSize: 12,
              fontFamily: 'Poppins-Medium',
              color: 'rgb(43,43,43)',
            }}>
            {this.props.title}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardsContainer: {
    flexDirection: 'row',
    borderColor: 'rgb(216,216,216)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 2,
    marginBottom: 6,
    overflow: 'hidden',
  },
  scoreContainer: {
    paddingVertical: 12,
    paddingHorizontal: 8,
    flexBasis: '18%',
    justifyContent: 'center',
  },
  titleContainer: {
    paddingVertical: 12,
    paddingHorizontal: 20,
    backgroundColor: 'rgb(247,247,247)',
    flexBasis: '82%',
    justifyContent: 'center',
  },
});
