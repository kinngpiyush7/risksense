describe('Test for Reconnaissance Overview Card in Dashboard', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Checking Reconnaissance Overview Card and its elements', async () => {
    await expect(element(by.id('reconnaissanceOverviewCard'))).toBeVisible();
    await expect(element(by.id('reconnaissanceOverview'))).toBeVisible();
    await expect(element(by.id('reconnaissanceOverview'))).toHaveText(
      'Reconnaissance Overview',
    );
    await expect(element(by.id('vennDiagram'))).toBeVisible();
    await expect(element(by.id('resultOfDomainCard'))).toBeVisible();
    await expect(element(by.id('resultOfDomain'))).toBeVisible();
    await expect(element(by.id('resultOfDomain'))).toHaveText(
      'Result of Domain - ',
    );
    await expect(element(by.id('resultOfDomainValue'))).toBeVisible();
    await expect(element(by.id('ipWithinDomain'))).toBeVisible();
    await expect(element(by.id('ipWithinDomain'))).toHaveText(
      'IP within Domain',
    );
    await expect(element(by.id('ipWithinDomainValue'))).toBeVisible();
    await expect(element(by.id('domainsWithoutIp'))).toBeVisible();
    await expect(element(by.id('domainsWithoutIp'))).toHaveText(
      'Domains without IP',
    );
    await expect(element(by.id('domainsWithoutIpValue'))).toBeVisible();
    await expect(element(by.id('subDomains'))).toBeVisible();
    await expect(element(by.id('subDomains'))).toHaveText('Sub Domains');
    await expect(element(by.id('subDomainsValue'))).toBeVisible();
    await expect(element(by.id('fraudulentDomains'))).toBeVisible();
    await expect(element(by.id('fraudulentDomains'))).toHaveText(
      'Fraudulent Domains',
    );
    await expect(element(by.id('fraudulentDomainsValue'))).toBeVisible();
    await expect(element(by.id('totalAssets'))).toBeVisible();
    await expect(element(by.id('totalAssets'))).toHaveText('Total Assets');
    await expect(element(by.id('totalAssetsValue'))).toBeVisible();
    await element(by.id('reconnaissanceOverview')).tap();
    await element(by.id('resultOfDomainCard')).tap();
  });
});
