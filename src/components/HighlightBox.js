import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const HighlightBox = props => {
  return (
    <View
      testID={props.testID}
      style={[
        styles.highlightBox,
        props.boxColor ? styles.blueBox : styles.redBox,
      ]}>
      <Text
        style={[
          styles.highlightText,
          props.boxColor ? styles.whiteText : styles.redText,
        ]}>
        {props.children}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  highlightBox: {
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    width: 68,
    height: 39,
  },
  blueBox: {
    backgroundColor: '#00B3C4',
  },
  redBox: {
    backgroundColor: 'rgb(255,245,247)',
  },
  highlightText: {
    justifyContent: 'center',
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    alignSelf: 'center',
  },
  whiteText: {
    color: '#fff',
  },
  redText: {
    color: 'rgb(198,18,39)',
  },
});

export default HighlightBox;
