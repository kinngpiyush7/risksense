import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class CustomDescriptionBox extends Component {
  render() {
    const {label, ...props} = this.props;
    return (
      <View>
        {/* Getting the Height of the Reusable DescBox via Props - Eli */}
        <View style={[styles.mainDesc2, props.height]}>
          <View
            style={[
              styles.mainDesc,
              props.height,
              props.mainDescStyle ? props.mainDescStyle : styles.padding,
            ]}>
            <View style={props.customDescStyle}>
              {/* Accessing the TestID passed by the parent component and using the text passed with key Header - Eli  */}
              <Text
                style={[
                  styles.mainHead,
                  {
                    fontSize: props.headerFontSize ? props.headerFontSize : 26,
                  },
                ]}
                testID={props.headerTest}>
                {props.header ? props.header : null}
              </Text>
              {props.text1 && (
                <Text style={styles.mainContent}>
                  {props.text1 ? props.text1 : null}
                </Text>
              )}
              {props.text2 && (
                <Text style={styles.mainContent_2}>
                  {props.text2 ? props.text2 : null}
                </Text>
              )}
            </View>
          </View>
        </View>
      </View>
    );
  }
}

// The default styles applied on the button if the properties are not passed - Eli
const styles = StyleSheet.create({
  mainDesc2: {
    backgroundColor: 'rgba(86,86,86,0.3)',
    width: '100%',
    position: 'absolute',
  },
  mainDesc: {
    backgroundColor: 'rgba(22,22,22,0.98)',
    position: 'relative',
    top: -15,
    left: -15,
  },
  padding: {
    paddingHorizontal: 70,
    paddingVertical: 36,
  },
  mainHead: {
    lineHeight: 36,
    color: '#FFF',
    fontFamily: 'Poppins-SemiBold',
  },
  mainContent: {
    marginTop: 34,
    marginBottom: 20,
    color: 'rgb(210,210,210)',
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    lineHeight: 25,
  },
  mainContent_2: {
    color: 'rgb(210,210,210)',
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    lineHeight: 26,
    letterSpacing: 0.03,
  },
});
