describe('Test for the Dashboard Screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Checking for all the elements in the dashboard', async () => {
    await expect(element(by.id('bigCompanyLogo'))).toBeVisible();
    await expect(element(by.id('compnanyDomainName'))).toBeVisible();
    await expect(element(by.id('currentDate'))).toBeVisible();
    await expect(element(by.id('checkNewDomain'))).toBeVisible();
    await expect(element(by.id('checkNewDomain'))).toHaveText(
      'Check New Domain?',
    );
    await expect(element(by.id('deeperScanReportButton'))).toBeVisible();
  });
});
