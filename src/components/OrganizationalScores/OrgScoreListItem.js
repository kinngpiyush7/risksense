import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import CircularProgress from './CircularProgress';

export const OrgScoreListItem = props => {
  return (
    <View
      style={[
        {
          paddingHorizontal: 8,
          backgroundColor:
            props.currentItem === props.listItem.title ? '#d8f8fb' : null,
        },
        props.listStyle && props.listStyle,
      ]}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          paddingVertical: 10,
          borderBottomWidth: 1,
          borderBottomColor: '#d8f8fb',
        }}>
        <View style={{flexBasis: '80%', paddingLeft: 8}}>
          <View style={{flexDirection: 'row', marginBottom: 0}}>
            <Text testID={props.listItem.titleTestId} style={styles.title}>
              {props.listItem.title}
            </Text>
            <Text testID={props.listItem.issueTestId} style={styles.issue}>
              <Text style={{fontFamily: 'Poppins-Medium'}}>
                {props.listItem.issue}
              </Text>{' '}
              Issue
            </Text>
          </View>
          <Text testID={props.listItem.descTestId} style={styles.desc}>
            {props.listItem.description}
          </Text>
        </View>
        <View style={{marginLeft: 'auto', paddingRight: 8}}>
          {props.currentItem !== props.listItem.title && (
            <CircularProgress
              maxValue="825"
              value={props.listItem.progress}
              small={true}
              positiveIncrease={false}
            />
          )}
        </View>
      </View>
    </View>
  );
};

const OrgScoreListItemTouchable = props => {
  return (
    <TouchableOpacity
      onPress={() => {
        props.changeCard(props.listItem.title);
      }}>
      <OrgScoreListItem {...props} />
    </TouchableOpacity>
  );
};

export default OrgScoreListItemTouchable;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 14,
    marginRight: 20,
    color: 'rgb(43,43,43)',
  },
  desc: {
    fontFamily: 'Poppins-Regular',
    fontSize: 10,
    color: 'rgb(86,86,86)',
  },
  issue: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    color: 'rgb(198,18,39)',
  },
});
