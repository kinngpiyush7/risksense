describe('Test for Application Report page', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Application Report page', async () => {
    await expect(element(by.id('App_backarrow_button'))).toBeVisible();
    await element(by.id('App_backarrow_button')).tap();
    await expect(element(by.id('App_backarrow'))).toBeVisible();
    await expect(element(by.id('App_reportimage'))).toBeVisible();
    await expect(element(by.id('App_heading'))).toBeVisible();
    await expect(element(by.id('App_heading'))).toHaveText(
      'Application Security Report',
    );
    await expect(element(by.id('App_sub_heading'))).toBeVisible();
    await expect(element(by.id('App_sub_heading'))).toHaveText(
      'Prevent from service attacks, cyberattacks and data breaches or data theft situations',
    );
    await expect(element(by.id('App_graphcontainer'))).toBeVisible();
    // await expect(element(by.id('App_table'))).toBeVisible();

    // await expect(element(by.id('App_bottomcontainer_text_1'))).toBeVisible();
    await expect(element(by.id('App_bottomcontainer_text_1'))).toHaveText(
      'Action items to improve the Organization Score -Disable critical services like Redis, MySQL, and RDP',
    );
    // await expect(element(by.id('App_bottomcontainer_text_2'))).toBeVisible();
    // await expect(element(by.id('App_bottomcontainer_text_2'))).toHaveText(
    //   '- Disable critical services like Redis, MySQL, and RDP',
    // );
  });
});
