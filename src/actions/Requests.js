import {ADD_REQUEST, ALL_REQUESTS} from './Types';

export const addNewRequest = request => {
  return {type: ADD_REQUEST, payload: request};
};

export const getAllRequests = () => {
  return {type: ALL_REQUESTS};
};
