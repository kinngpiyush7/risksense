import React, {Component} from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import HighlightBox from '../components/HighlightBox';

export default class TopFixes extends Component {
  render() {
    return (
      <View style={styles.cardContainer}>
        <View testID="topFixesCard" style={styles.cardInterior}>
          <Text testID="topFixes" style={styles.title}>
            Top Fixes & Categories
          </Text>
          <View testID="circleCard" style={styles.circleCard}>
            <Text testID="problemsFoundValue" style={styles.totalProblems}>
              <Text style={styles.totalProblemsPlus}>+</Text>172
            </Text>
            <Text testID="problems" style={styles.problems}>
              problems
            </Text>
            <Text testID="found" style={styles.problems}>
              found
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 12, marginLeft: 15}}>
            <HighlightBox testID="vulnerabilityValue">+114</HighlightBox>
            <Text testID="vulnerability" style={styles.problemTypes}>
              Vulnerability Related
            </Text>
          </View>
          <View style={{flexDirection: 'row', marginTop: 8, marginLeft: 15}}>
            <HighlightBox testID="configValue">+58</HighlightBox>
            <Text testID="config" style={styles.problemTypes}>
              Configuration Related
            </Text>
          </View>
          <View
            style={{
              borderBottomColor: 'rgb(216,216,216)',
              borderBottomWidth: 1,
              paddingBottom: 6,
            }}>
            <Text testID="topActions" style={styles.topActions}>
              Top Remediation Actions
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 11,
            }}>
            <View>
              <Text testID="spfPractices" style={styles.actionsName}>
                SPF Best Practises not followed
              </Text>
              <Text testID="patchingCadence" style={styles.actionsName}>
                Patching Cadence for CVE-2014-4078
              </Text>
              <Text testID="vulnarability" style={styles.actionsName}>
                Vulnerability: CVE 2014-3456
              </Text>
              <Text testID="dnsecEntry" style={styles.actionsName}>
                DNSEC Entry Missing for Domain
              </Text>
              <Text testID="openPort443" style={styles.actionsName}>
                Open Port 443 is detected
              </Text>
              <Text testID="sdaPractices" style={styles.actionsName}>
                SDA Best Practises not followed
              </Text>
              <Text testID="openPort8907" style={styles.actionsName}>
                Open Port 8907 is detected
              </Text>
            </View>
            <View>
              <Text testID="spfPracticesValue" style={styles.actionsValue}>
                +45
              </Text>
              <Text testID="patchingCadenceValue" style={styles.actionsValue}>
                +39
              </Text>
              <Text testID="vulnarabilityValue" style={styles.actionsValue}>
                +26
              </Text>
              <Text testID="dnsecEntryValue" style={styles.actionsValue}>
                +15
              </Text>
              <Text testID="openPort443Value" style={styles.actionsValue}>
                +13
              </Text>
              <Text testID="sdaPracticesValue" style={styles.actionsValue}>
                +7
              </Text>
              <Text testID="openPort8907Value" style={styles.actionsValue}>
                +3
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 25,
  },
  cardInterior: {
    borderColor: '#ededed',
    borderWidth: 2,
    borderRadius: 4,
    padding: 15,
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    color: 'rgb(43,43,43)',
    fontSize: 16,
    marginBottom: 20,
  },
  circleCard: {
    borderColor: 'rgb(227,92,108)',
    borderWidth: 3,
    width: 104,
    height: 104,
    borderRadius: 104 / 2,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  totalProblemsPlus: {
    fontSize: 23,
    letterSpacing: 0.6,
    lineHeight: 32,
    color: 'rgb(43,43,43)',
    fontFamily: 'Poppins-Bold',
  },
  totalProblems: {
    fontSize: 30,
    letterSpacing: 0.7,
    lineHeight: 32,
    color: 'rgb(43,43,43)',
    fontFamily: 'Poppins-SemiBold',
  },
  problems: {
    color: 'rgb(227,92,108)',
    fontSize: 11,
    lineHeight: 14,
    fontFamily: 'Poppins-Medium',
  },
  problemTypes: {
    marginTop: 9,
    marginLeft: 15,
    fontSize: 13,
    letterSpacing: 0.46,
    color: 'rgb(43,43,43)',
    fontFamily: 'Poppins-SemiBold',
  },
  topActions: {
    fontSize: 14,
    color: '#C91E32',
    fontFamily: 'Poppins-SemiBold',
    marginTop: 20,
  },
  actionsName: {
    color: 'rgb(43,43,43)',
    fontSize: 12,
    lineHeight: 27,
    fontFamily: 'Poppins-Medium',
  },
  actionsValue: {
    color: 'rgb(198,18,39)',
    fontSize: 12,
    lineHeight: 27,
    fontFamily: 'Poppins-SemiBold',
  },
});
