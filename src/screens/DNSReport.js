import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import ReportTable from '../components/ReportTable';
import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import {SafeAreaView} from 'react-navigation';

import Gauge from '../components/OrganizationalScores/Gauge';
import CircularProgress from '../components/OrganizationalScores/CircularProgress';
import {widthPercentageToDP} from '../components/Responsive';

import CustomButton from '../components/CustomButton';
import {mediumButton} from '../assets/styles/Buttons';
import Tag from '../components/OrganizationalScores/Tag';
import DNS_image from '../assets/images/dnsSecurity/srsIconsDnsSecurity.png';

export default class DNSReport extends Component {
  static navigationOptions = {
    header: null,
  };

  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };
  render() {
    return (
      <SafeAreaView>
        <ScrollView style={{height: Dimensions.get('window').height}}>
          <View testID="DNS_container" style={styles.DNS_container}>
            <View testID="DNS_header" style={styles.header}>
              <TouchableOpacity
                testID="DNS_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="DNS_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="DNS_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={DNS_image}></Image>
              <View testID="DNS_header_texts" style={styles.header_texts}>
                <Text testID="DNS_heading" style={styles.heading}>
                  DNS Security
                </Text>
                <Text testID="DNS_sub_heading" style={styles.sub_heading}>
                  Used to rotect information on the Domain Name System
                </Text>
              </View>
            </View>
            <View testID="DNS_graphcontainer" style={styles.DNS_graphcontainer}>
              {/* graphs view */}

              <View
                style={{
                  marginRight: 30,
                  borderRightWidth: 1,
                  width: 135,
                  borderColor: '#D2D2D2',
                }}>
                <CircularProgress
                  maxValue="1765"
                  value="890"
                  small={false}
                  positiveIncrease={false}
                  onlyBenchmark={true}
                />
              </View>
              <View style={{marginRight: 30}}>
                <Gauge gaugeScore="601" gaugeType="DNS Security" />
              </View>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{paddingRight: 15}}>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      Critical
                    </Text>
                    <View
                      style={{
                        backgroundColor: '#BE1E2C',
                        marginTop: 4,
                        marginBottom: 2,
                        width: 40,
                        height: 4,
                      }}></View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      300-600
                    </Text>
                  </View>
                  <View style={{paddingRight: 15}}>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      High
                    </Text>
                    <View
                      style={{
                        backgroundColor: '#F7941E',
                        marginTop: 4,
                        marginBottom: 2,
                        width: 40,
                        height: 4,
                      }}></View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      600-650
                    </Text>
                  </View>
                  <View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      Info
                    </Text>
                    <View
                      style={{
                        backgroundColor: '#00B3C4',
                        marginTop: 4,
                        marginBottom: 2,
                        width: 40,
                        height: 4,
                      }}></View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      600-650
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{paddingRight: 15}}>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      Medium
                    </Text>
                    <View
                      style={{
                        backgroundColor: '#FBCE3F',
                        marginTop: 4,
                        marginBottom: 2,
                        width: 40,
                        height: 4,
                      }}></View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      650-750
                    </Text>
                  </View>
                  <View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      Low
                    </Text>
                    <View
                      style={{
                        backgroundColor: '#39B449',
                        marginTop: 4,
                        marginBottom: 2,
                        width: 40,
                        height: 4,
                      }}></View>
                    <Text style={{fontSize: 9, fontFamily: 'Poppins-Medium'}}>
                      750-800
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View testID="DNS_table" style={styles.DNS_table}>
              {/* calling te reuseable table component and passing the required credential */}
              <ReportTable
                showLegend={true}
                tableMargin={{marginHorizontal: 0}}
                heading="The DNS scurity component represents the overall health of the client's DNS settings as gathered from passive DNS settings. The underlying methodoogy evaluates the SOA records, DNSSec. settings for best practics and furthe evaluates the clients domains' susceptibility to typosquatting and cybersquatting attacks."
                column_names={[
                  'Control ID',
                  'Finding',
                  'Section',
                  'Description',
                ]}
                widthArr={[
                  widthPercentageToDP('14'),
                  widthPercentageToDP('30'),
                  widthPercentageToDP('16'),
                  widthPercentageToDP('32'),
                ]}
                column_data={[
                  [
                    <View style={{flexDirection: 'row'}}>
                      <View style={{marginRight: 4}}>
                        <Tag
                          tag={'DNS-10'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#00b3c4',
                            borderColor: '#00b3c4',
                          }}
                        />
                      </View>
                      <View>
                        <Tag
                          tag={'CWE-213'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#ff8930',
                            borderColor: '#ff8930',
                          }}
                        />
                      </View>
                    </View>,
                    <Text style={{fontFamily: 'Poppins-Medium'}}>
                      SOA EXPIRE property and TTL value not in recommended range
                    </Text>,
                    'Parent Servers Tests',
                    <Text style={{color: 'rgb(48,48,48)'}}>
                      Cross-site request forgery (CSRF) vulnerabilies may arise
                      when applications rely soley
                    </Text>,
                  ],
                  [
                    <View style={{flexDirection: 'row'}}>
                      <View style={{marginRight: 4}}>
                        <Tag
                          tag={'CWE-10'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#00b3c4',
                            borderColor: '#00b3c4',
                          }}
                        />
                      </View>
                      <View>
                        <Tag
                          tag={'DNS-213'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#f2b924',
                            borderColor: '#f2b924',
                          }}
                        />
                      </View>
                    </View>,

                    <Text style={{fontFamily: 'Poppins-Medium'}}>
                      The parent server a.gtldservers.net is sending out GLUE
                      for nameserves
                    </Text>,
                    'RS Tests',
                    <Text style={{color: 'rgb(48,48,48)'}}>
                      Cross-site request forgery (CSRF) vulnerabilies may arise
                      when applications rely soley
                    </Text>,
                  ],
                  [
                    <Tag
                      tag={'CWE-10'}
                      customText={{fontSize: 10}}
                      customStyle={{
                        width: 60,
                        backgroundColor: '#c61227',
                        borderColor: '#c61227',
                      }}
                    />,
                    <Text style={{fontFamily: 'Poppins-Medium'}}>
                      {
                        'ns61.domaincontrol.com\n[97.74.101.32] [NO-GLUE] [TTL=3600]'
                      }
                    </Text>,
                    'Parent Servers Tests',
                    <Text style={{color: 'rgb(48,48,48)'}}>
                      Cross-site request forgery (CSRF) vulnerabilies may arise
                      when applications rely soley
                    </Text>,
                  ],
                  [
                    <View style={{flexDirection: 'row'}}>
                      <View style={{marginRight: 4}}>
                        <Tag
                          tag={'CWE-10'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#f2b924',
                            borderColor: '#f2b924',
                          }}
                        />
                      </View>
                      <View>
                        <Tag
                          tag={'CWE-10'}
                          customText={{fontSize: 10}}
                          customStyle={{
                            width: 60,
                            backgroundColor: '#00b3c4',
                            borderColor: '#00b3c4',
                          }}
                        />
                      </View>
                    </View>,

                    <Text style={{fontFamily: 'Poppins-Medium'}}>
                      Your Nameservers(from the local server) has A records
                    </Text>,
                    'RS Tests',
                    <Text style={{color: 'rgb(48,48,48)'}}>
                      Cross-site request forgery (CSRF) vulnerabilies may arise
                      when applications rely soley
                    </Text>,
                  ],
                  [
                    <Tag
                      tag={'DNS-10'}
                      customText={{fontSize: 10}}
                      customStyle={{
                        width: 60,
                        backgroundColor: '#ff8930',
                        borderColor: '#ff8930',
                      }}
                    />,
                    <Text style={{fontFamily: 'Poppins-Medium'}}>
                      {`162.02.229.110\n167.105.229.42`}
                    </Text>,
                    'RS Tests',
                    <Text style={{color: 'rgb(48,48,48)'}}>
                      Cross-site request forgery (CSRF) vulnerabilies may arise
                      when applications rely soley
                    </Text>,
                  ],
                ]}
              />
            </View>
            <View
              testID="PR_bottomcontainer"
              style={{
                marginTop: 20,
                marginBottom: 30,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                testID="DNS_bottomcontainer_text"
                style={{flexShrink: 1, width: 400}}>
                <Text
                  testID="DNS_bottomcontainer_text_1"
                  style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                  Action items to improve the Organization Score -
                  <Text
                    testID="DNS_bottomcontainer_text_2"
                    style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                    Disable critical services like Redis, MySQL, and RDP
                  </Text>
                </Text>
              </View>
              <View style={{marginLeft: 20}}>
                <CustomButton
                  buttonTest="startButton"
                  buttonBackgroundColor={false}
                  textColor={true}
                  hasShadow={true}
                  navigation={this.props.navigation.navigate}
                  handleButtonPress={this.handleNavigation}
                  textStyle={mediumButton.buttonTextStyle}
                  viewStyle={mediumButton.buttonViewStyle}
                  handleButtonPress={this.navigation}>
                  Get a Deeper Scan Report
                </CustomButton>
              </View>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  DNS_container: {
    padding: 15,
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  DNS_graphcontainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,

    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 6,

    marginLeft: 20,
  },
});
