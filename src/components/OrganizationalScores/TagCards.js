import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Tag from './Tag';

const TagCards = props => {
  return (
    <View style={styles.cardsContainer}>
      <View style={[styles.titleContainer]}>
        <Text
          style={{
            color: 'rgb(43,43,43)',
            textAlign: 'left',
            fontSize: 12,
            fontFamily: 'Poppins-Medium',
          }}>
          {props.title}
        </Text>
      </View>
      <View style={styles.tagsContainer}>
        {props.tags.map(tag => (
          <Tag
            tag={tag.tagTitle}
            customStyle={{
              marginRight: 4,
              backgroundColor: tag.bgColor,
              borderColor: tag.borderColor,
            }}
            customText={{lineHeight: 12, fontSize: 10}}
          />
        ))}
      </View>
    </View>
  );
};

export default TagCards;

const styles = StyleSheet.create({
  cardsContainer: {
    borderColor: 'rgb(216,216,216)',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 2,
    marginBottom: 6,
    overflow: 'hidden',
    paddingHorizontal: 10,
    paddingVertical: 8,
    backgroundColor: '#f7f7f7',
  },
  titleContainer: {
    marginBottom: 12,
  },
  tagsContainer: {
    flexDirection: 'row',
  },
});
