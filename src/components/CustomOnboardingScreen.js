import React from 'react';
import {View, StyleSheet, Dimensions, Image, Text} from 'react-native';
import CustomDescriptionBox from './CustomDescriptionBox';
import {heightPercentageToDP} from './Responsive';

const CustomOnboardingScreen = props => {
  return (
    // Declaring two Views overlapping each other - Eli
    <View style={styles.mainView}>
      <View style={styles.main}>
        {/* Declaring certain margins along with width, height - Eli  */}
        <View
          style={[
            styles.image_size,
            props.imageStyle ? props.imageStyle : styles.image_style,
          ]}
          testID={props.onboardingTest}>
          {/* Using the logo image and using custom styles - Eli  */}
          <Image
            testID="onboard_image"
            style={styles.image}
            source={require('../assets/images/risksense-logo/logo.png')}
          />
        </View>
        <View style={styles.sample}>
          {/* Using the Reusable Description Box on the Reusable Screen - Eli  */}
          {/* Using all the values from the props - Eli  */}
          <CustomDescriptionBox
            headerTest="onboardId"
            header={props.header ? props.header : null}
            height={props.height}
            headerFontSize={props.headerFontSize}
            mainDescStyle={props.mainDescStyle}
            customDescStyle={{marginTop: 0}}
          />
        </View>
      </View>
      {/* Declaring space for the white form container - Eli  */}
      {/* Using the styles via props or using custom styles - Eli  */}
      <View style={props.main2 ? styles.main2_demo : styles.main2}>
        {/* The Children property is accessed in this case - Eli */}
        {props.children}
      </View>
    </View>
  );
};

// The default styles applied on the button if the properties are not passed - Eli
const styles = StyleSheet.create({
  mainView: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: Dimensions.get('window').height,
  },
  image_style: {
    marginLeft: 85,
    marginTop: 42,
  },
  image_size: {
    width: 211,
    height: 62,
  },
  image: {
    width: '100%',
    height: '100%',
    alignItems: 'flex-start',
  },
  main: {
    flexBasis: '38%',
  },
  sample: {
    marginTop: heightPercentageToDP('20'),
    marginBottom: 'auto',
  },
  main2: {
    flexBasis: '48.5%',
    paddingHorizontal: 36,
    paddingTop: 40,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    height: heightPercentageToDP('93'),
  },
  main2_demo: {
    flexBasis: '48.5%',
    paddingHorizontal: 36,
    paddingTop: 40,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 5,
    marginTop: 'auto',
    marginBottom: 'auto',
    height: heightPercentageToDP('75'),
  },
});

export default CustomOnboardingScreen;
