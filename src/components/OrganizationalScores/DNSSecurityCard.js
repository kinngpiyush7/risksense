import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import ScoresHeading from './ScoresHeading';
import TagCards from './TagCards';
import Gauge from './Gauge';
import CircularProgress from './CircularProgress';
import CustomButton from '../CustomButton';

import {smallButton, cardButton} from '../../assets/styles/Buttons';

const scoreData = [
  {
    tags: [
      {tagTitle: 'CWE - 10', bgColor: '#9E0A1B', borderColor: '#9E0A1B'},
      {
        tagTitle: 'DNS - 10',
        bgColor: 'rgb(0,179,196)',
        borderColor: 'rgb(0,179,196)',
      },
    ],
    tagsTitle: 'SOA EXPIRE property and TTL value not in recommended range',
  },
  {
    tags: [
      {
        tagTitle: 'DNS - 10',
        bgColor: 'rgb(251,206,63)',
        borderColor: 'rgb(251,206,63)',
      },
    ],
    tagsTitle: 'ns61.domaincontrol.com [97.74.101.32] [NO GLUE] [TTL=3600]',
  },
  {
    tags: [
      {
        tagTitle: 'CWE - 10',
        bgColor: 'rgb(255,137,48)',
        borderColor: 'rgb(255,137,48)',
      },
    ],
    tagsTitle: '167.102.229.110',
  },
  {
    tags: [
      {
        tagTitle: 'CWE - 8',
        bgColor: 'rgb(251,206,63)',
        borderColor: 'rgb(251,206,63)',
      },
    ],
    tagsTitle: '167.102.229.42',
  },
];

const DNSSecurityCard = props => {
  return (
    <View style={styles.cardContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 14,
        }}>
        <Text
          testID="dnsSecCardText"
          style={{
            fontFamily: 'Poppins-SemiBold',
            color: 'rgb(43,43,43)',
            marginTop: 12,
            fontSize: 14,
          }}>
          DNS Security
        </Text>
        <TouchableOpacity onPress={() => props.activeScreen('')}>
          <View style={{paddingHorizontal: 14, paddingVertical: 12}}>
            <Text
              testID="closeIcon"
              style={{
                color: 'rgb(0,179,196)',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              X
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* graphs view */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 6,
          // marginTop: 6,
          marginBottom: 20,
          marginHorizontal: 14,
        }}>
        <CircularProgress maxValue="1765" value="890" small={false} />
        <Gauge gaugeScore="601" gaugeType="DNS Security" />
      </View>
      <View style={{marginHorizontal: 14}}>
        <ScoresHeading headingTextId="dnsSecCardHeaderText">
          Top Findings
        </ScoresHeading>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 12,
          }}>
          <Text
            style={{
              fontFamily: 'Poppins-Medium',
              fontSize: 12,
              marginRight: 20,
            }}>
            CWE
          </Text>
          <Text
            testID="dnsSecCWE"
            style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
            Common Weakness Enumeration
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            marginTop: 0,
          }}>
          <Text
            style={{
              fontFamily: 'Poppins-Medium',
              fontSize: 12,
              marginRight: 20,
            }}>
            DNS
          </Text>
          <Text
            testID="dnsSecDNS"
            style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
            Domain Name System
          </Text>
        </View>
        <View style={{height: 354, overflow: 'hidden'}}>
          <FlatList
            style={{marginTop: 12}}
            data={scoreData}
            renderItem={({item, index}) => (
              <TagCards title={item.tagsTitle} tags={item.tags} />
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
      </View>
      <View style={cardButton}>
        <CustomButton
          buttonTest="dnsSecDetailReportButton"
          handleButtonPress={() => props.navigate('DNSReport')}
          buttonBackgroundColor={true}
          textColor={false}
          hasShadow={true}
          textStyle={smallButton.buttonTextStyle}
          viewStyle={smallButton.buttonViewStyle}
          navigation={() => {}}>
          Detail Report
        </CustomButton>
      </View>
    </View>
  );
};

export default DNSSecurityCard;

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: '#00b3c4',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    paddingBottom: 14,
    height: '100%',
  },
});
