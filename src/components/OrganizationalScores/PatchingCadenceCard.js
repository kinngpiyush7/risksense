import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import ScoresHeading from './ScoresHeading';
import ScoreCards from './ScoreCards';
import Gauge from './Gauge';
import CircularProgress from './CircularProgress';
import CustomButton from '../CustomButton';

import {smallButton, cardButton} from '../../assets/styles/Buttons';

const scoreData = [
  {
    score: 7,
    scoreTitle: 'CVE-2017-3167',
  },
  {
    score: 3,
    scoreTitle: 'CVE-2016-0736',
  },
  {
    score: 7,
    scoreTitle: 'CVE-2015-0253',
  },
  {
    score: 3,
    scoreTitle: 'CVE-2016-4975',
  },
  {
    score: 2,
    scoreTitle: 'CVE-2015-0253',
  },
  {
    score: 3,
    scoreTitle: 'CVE-2016-4975',
  },
  {
    score: 1,
    scoreTitle: 'CVE-2016-4975',
  },
];

const PatchingCadenceCard = props => {
  return (
    <View style={styles.cardContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 14,
        }}>
        <Text
          testID="patchCadCardText"
          style={{
            fontFamily: 'Poppins-SemiBold',
            color: 'rgb(43,43,43)',
            marginTop: 12,
            fontSize: 14,
          }}>
          Patching Cadence
        </Text>
        <TouchableOpacity onPress={() => props.activeScreen('')}>
          <View style={{paddingHorizontal: 14, paddingVertical: 12}}>
            <Text
              testID="closeIcon"
              style={{
                color: 'rgb(0,179,196)',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              X
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* graphs view */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 6,
          marginBottom: 20,
          marginHorizontal: 14,
        }}>
        <CircularProgress maxValue="3569" value="2312" small={false} />
        <Gauge gaugeScore="481" gaugeType="Patch Cadence" />
      </View>
      <View style={{marginHorizontal: 14}}>
        <ScoresHeading headingTextId="patchCadCardHeaderText">
          Need to Patch
        </ScoresHeading>
        <Text
          testID="patchCadDescription"
          style={{
            fontFamily: 'Poppins-Regular',
            fontSize: 11,
            marginTop: 12,
            color: 'rgb(43,43,43)',
          }}>
          The analysis is performed by passively gathering asset fingerprint
          details
        </Text>
        <View style={{height: 362, overflow: 'hidden'}}>
          <FlatList
            style={{marginTop: 12}}
            data={scoreData}
            renderItem={({item, index}) => (
              <ScoreCards
                dynamicColor={true}
                scoreCardScoreId={`patchCadScoreCardScore-${index}`}
                scoreCardTitleID={`patchCadScoreCardTitle-${index}`}
                title={item.scoreTitle}
                score={item.score}
              />
            )}
            keyExtractor={item => item.scoreTitle}
          />
        </View>
      </View>
      <View style={cardButton}>
        <CustomButton
          buttonTest="patchCadDetailReportButton"
          handleButtonPress={() => props.navigate('PatchReport')}
          buttonBackgroundColor={true}
          textColor={false}
          hasShadow={true}
          textStyle={smallButton.buttonTextStyle}
          viewStyle={smallButton.buttonViewStyle}
          navigation={() => {}}>
          Detail Report
        </CustomButton>
      </View>
    </View>
  );
};

export default PatchingCadenceCard;

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: '#00b3c4',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    paddingBottom: 14,
    height: '100%',
  },
});
