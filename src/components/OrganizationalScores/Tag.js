import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

export default class Tag extends Component {
  render() {
    return (
      <View
        style={[
          styles.tag,
          this.props.customStyle ? this.props.customStyle : null,
        ]}>
        <Text
          style={[
            styles.tagText,
            this.props.customText ? this.props.customText : null,
          ]}>
          {this.props.tag}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tag: {
    paddingTop: 2,
    paddingHorizontal: 6,
    borderWidth: 1,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tagText: {
    color: '#ffffff',
    fontFamily: 'Poppins-Medium',
  },
});
