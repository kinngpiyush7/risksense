import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import ScoresHeading from './ScoresHeading';
import ScoreCards from './ScoreCards';
import Gauge from './Gauge';
import CircularProgress from './CircularProgress';
import CustomButton from '../CustomButton';

import {smallButton, cardButton} from '../../assets/styles/Buttons';

const scoreData = [
  {
    score: 7,
    scoreTitle: 'POODLE Vulnerability',
  },
  {
    score: 127,
    scoreTitle: 'DROWN Vulnerability',
  },
  {
    score: 23,
    scoreTitle: 'SSL Certificate not PCIDSS Complaint',
  },
  {
    score: 0,
    scoreTitle: 'CVE-2014-0224',
  },
  {
    score: 33,
    scoreTitle: 'CVE-2016-2107',
  },
  {
    score: 12,
    scoreTitle: 'SSL Certificate Has Weak Signature Algorithm',
  },
];

const ApplicationSecurityCard = props => {
  return (
    <View style={styles.cardContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 14,
        }}>
        <Text
          testID="appSecCardText"
          style={{
            fontFamily: 'Poppins-SemiBold',
            color: 'rgb(43,43,43)',
            marginTop: 12,
            fontSize: 14,
          }}>
          Application Security
        </Text>
        <TouchableOpacity onPress={() => props.activeScreen('')}>
          <View style={{paddingHorizontal: 14, paddingVertical: 12}}>
            <Text
              testID="closeIcon"
              style={{
                color: 'rgb(0,179,196)',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              X
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* graphs view conatiner */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 6,
          marginBottom: 20,
          marginHorizontal: 14,
        }}>
        <CircularProgress maxValue="1335" value="359" small={false} />
        <Gauge gaugeScore="652" gaugeType="Application" />
      </View>
      <View style={{marginHorizontal: 14}}>
        <ScoresHeading headingTextId="appSecCardHeaderText">
          High Risk Vulnerabilities
        </ScoresHeading>
        {/* score cards with scroll view */}
        <View style={{height: 400, overflow: 'hidden'}}>
          <FlatList
            style={{marginTop: 12, height: 400}}
            data={scoreData}
            renderItem={({item, index}) => (
              <ScoreCards
                dynamicColor={true}
                scoreCardScoreId={`appSecScoreCardScore-${index}`}
                scoreCardTitleID={`appSecScoreCardTitle-${index}`}
                title={item.scoreTitle}
                score={item.score}
              />
            )}
            keyExtractor={item => item.scoreTitle}
          />
        </View>
      </View>
      <View style={cardButton}>
        <CustomButton
          buttonTest="appSecDetailReportButton"
          handleButtonPress={() => props.navigate('ApplicationReport')}
          buttonBackgroundColor={true}
          textColor={false}
          hasShadow={true}
          textStyle={smallButton.buttonTextStyle}
          viewStyle={smallButton.buttonViewStyle}
          navigation={() => {}}>
          Detail Report
        </CustomButton>
      </View>
    </View>
  );
};

export default ApplicationSecurityCard;

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: '#00b3c4',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    paddingBottom: 14,
    height: '100%',
    position: 'relative',
  },
});
