describe('Test for network security page', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('network security page', async () => {
    await expect(element(by.id('NR_backarrow_button'))).toBeVisible();
    await element(by.id('NR_backarrow_button')).tap();
    await expect(element(by.id('NR_backarrow'))).toBeVisible();
    await expect(element(by.id('NR_heading'))).toBeVisible();
    await expect(element(by.id('NR_heading'))).toHaveText(
      'Network Security Report',
    );
    await expect(element(by.id('NR_sub_heading'))).toBeVisible();
    await expect(element(by.id('NR_sub_heading'))).toHaveText(
      'An integration of multiple layers of defenses in the network and at the network',
    );

    await expect(element(by.id('NR_graphcontainer_heading_text'))).toHaveText(
      'Summarizes the top ports open in the target infrastructure based on the number of assets on which they are open',
    );
    // await expect(element(by.id('NR_bottomcontainer_text_1'))).toBeVisible();
    await expect(element(by.id('NR_bottomcontainer_text_1'))).toHaveText(
      'Action items to improve the Organization Score -Disable critical services like Redis, MySQL, and RDP',
    );
    // await expect(element(by.id('NR_bottomcontainer_text_2'))).toBeVisible();
    // await expect(element(by.id('NR_bottomcontainer_text_2'))).toHaveText(
    //   '- Disable critical services like Redis, MySQL, and RDP',
    // );
  });
});
