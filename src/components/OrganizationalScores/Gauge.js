import React, {Component} from 'react';
import {View} from 'react-native';
import Svg, {
  Circle,
  Text as SvgText,
  TSpan,
  G,
  Line,
  Image,
} from 'react-native-svg';

const colorsArray = [
  'rgb(121,184,79)',
  'rgb(251,206,63)',
  'rgb(255,137,48)',
  'rgb(213,62,79)',
  'rgb(158,10,27)',
];
const r = 58;
const size = 65;

export default class Gauge extends Component {
  state = {
    currentColor: '',
    gaugeLevel: '',
  };
  componentDidMount() {
    if (this.props.gaugeScore === 0 && this.props.gaugeScore <= 30)
      this.setState({currentColor: colorsArray[0], gaugeLevel: 'Low'});
    else if (this.props.gaugeScore >= 31 && this.props.gaugeScore <= 200)
      this.setState({currentColor: colorsArray[1], gaugeLevel: 'Medium'});
    else if (this.props.gaugeScore >= 201 && this.props.gaugeScore <= 500)
      this.setState({currentColor: colorsArray[2], gaugeLevel: 'Neutral'});
    else if (this.props.gaugeScore >= 501 && this.props.gaugeScore <= 800)
      this.setState({currentColor: colorsArray[3], gaugeLevel: 'High Risk'});
    else if (this.props.gaugeScore >= 801)
      this.setState({currentColor: colorsArray[4], gaugeLevel: 'Critical'});
  }
  render() {
    return (
      <View>
        <Svg style={{width: size * 2 + 4, height: r + 14 + 12}}>
          <Circle
            cx={size}
            cy={size}
            r={r}
            stroke={this.state.currentColor}
            strokeWidth="14"
            fill="none"
            strokeDasharray={`${Math.PI * r}`}
            strokeDashoffset={`${Math.PI * r}`}
          />
          <G x={size} y={size}>
            <SvgText textAnchor="middle">
              <TSpan
                x={0}
                y={-22}
                fontFamily="Poppins-SemiBold"
                fill="rgb(46,54,56)"
                fontWeight={800}
                fontSize="18">
                {this.props.gaugeScore}
              </TSpan>
              <TSpan
                x={0}
                y={-8}
                fontFamily="Poppins-Medium"
                fontWeight={600}
                fill="rgb(107,107,107)"
                fontSize="11">
                {this.state.gaugeLevel}
              </TSpan>
              {this.props.gaugeType === 'Network' && (
                <>
                  <TSpan
                    x={26}
                    y={-22}
                    fontFamily="Poppins-Regular"
                    fill="rgb(213,62,79)"
                    fontSize="8">
                    34
                  </TSpan>
                  <Image
                    x={22}
                    y={-66}
                    width={7}
                    href={require('../../assets/images/down-arrow.png')}
                  />
                </>
              )}
            </SvgText>
            <Line
              x1="-24"
              y1="0"
              x2="24"
              y2="0"
              stroke="rgb(196,196,196)"
              strokeWidth="1"
            />
          </G>
          <G>
            <SvgText
              x={5}
              y={r + 20}
              fontFamily="Poppins-Regular"
              color="rgb(46,54,56)"
              fontSize="10">
              0
            </SvgText>
            <SvgText
              x={size}
              y={r + 20}
              textAnchor="middle"
              fontFamily="Poppins-Medium"
              color="rgb(46,46,46)"
              fontWeight={600}
              fontSize="10">
              {this.props.gaugeType}
            </SvgText>
            <SvgText
              x={2 * r + 8}
              y={r + 20}
              textAnchor="middle"
              fontFamily="Poppins-Regular"
              color="rgb(46,54,56)"
              fontSize="10">
              825
            </SvgText>
          </G>
        </Svg>
      </View>
    );
  }
}
