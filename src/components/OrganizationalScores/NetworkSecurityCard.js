import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

import ScoresHeading from './ScoresHeading';
import ScoreCards from './ScoreCards';
import Gauge from './Gauge';
import CircularProgress from './CircularProgress';
import CustomButton from '../CustomButton';

import {smallButton, cardButton} from '../../assets/styles/Buttons';

const scoreData = [
  {
    score: 2389,
    scoreTitle: 'Redis service observed',
  },
  {
    score: 127,
    scoreTitle: 'MySQL service observed',
  },
  {
    score: 6379,
    scoreTitle: 'Microsoft SQL Server service observed',
  },
  {
    score: 5900,
    scoreTitle: 'PostgresSQL Service observed',
  },
  {
    score: 33,
    scoreTitle: 'IMAP service observed',
  },
];

const NetworkSecurityCard = props => {
  return (
    <View style={styles.cardContainer}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginLeft: 14,
        }}>
        <Text
          testID="netSecCardText"
          style={{
            fontFamily: 'Poppins-SemiBold',
            color: 'rgb(43,43,43)',
            marginTop: 14,
            fontSize: 14,
          }}>
          Network Security
        </Text>
        <TouchableOpacity onPress={() => props.activeScreen('')}>
          <View style={{paddingHorizontal: 14, paddingVertical: 12}}>
            <Text
              testID="closeIcon"
              style={{
                color: 'rgb(0,179,196)',
                fontFamily: 'Poppins-Medium',
                fontSize: 16,
              }}>
              X
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {/* graphs view */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 6,
          // marginTop: 6,
          marginBottom: 20,
          marginHorizontal: 14,
        }}>
        <CircularProgress maxValue="1335" value="359" small={false} />
        <Gauge gaugeScore="549" gaugeType="Network" />
      </View>
      <View style={{marginHorizontal: 14}}>
        <ScoresHeading headingTextId="netSecCardHeaderText">
          High Risk Assets
        </ScoresHeading>
        <Text
          testID="netSecDescription"
          style={{
            fontFamily: 'Poppins-Regular',
            fontSize: 11,
            marginTop: 12,
            color: 'rgb(43,43,43)',
          }}>
          Open Port
        </Text>
        <View style={{height: 374, overflow: 'hidden'}}>
          <FlatList
            style={{marginTop: 12}}
            data={scoreData}
            renderItem={({item, index}) => (
              <ScoreCards
                dynamicColor={false}
                scoreCardScoreId={`netSecScoreCardScore-${index}`}
                scoreCardTitleID={`netSecScoreCardTitle-${index}`}
                title={item.scoreTitle}
                score={item.score}
              />
            )}
            keyExtractor={item => item.scoreTitle}
          />
        </View>
      </View>
      <View style={cardButton}>
        <CustomButton
          buttonTest="netSecDetailReportButton"
          handleButtonPress={() => props.navigate('NetworkReport')}
          buttonBackgroundColor={true}
          textColor={false}
          hasShadow={true}
          textStyle={smallButton.buttonTextStyle}
          viewStyle={smallButton.buttonViewStyle}
          navigation={() => {}}>
          Detail Report
        </CustomButton>
      </View>
    </View>
  );
};

export default NetworkSecurityCard;

const styles = StyleSheet.create({
  cardContainer: {
    borderColor: '#00b3c4',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 6,
    paddingBottom: 14,
    height: '100%',
  },
});
