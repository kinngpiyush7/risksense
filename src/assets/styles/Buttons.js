import {StyleSheet} from 'react-native';

const largeButton = StyleSheet.create({
  buttonViewStyle: {
    paddingVertical: 16,
    paddingHorizontal: 52,
  },
  buttonTextStyle: {
    fontSize: 16,
    letterSpacing: 1,
  },
});

const smallButton = StyleSheet.create({
  buttonViewStyle: {
    paddingVertical: 10,
    paddingHorizontal: 30,
  },
  buttonTextStyle: {
    fontSize: 12,
    // letterSpacing: 1,
  },
});

const cardButton = {
  justifyContent: 'center',
  alignItems: 'center',
  marginTop: 20,
  width: '100%',
  position: 'absolute',
  bottom: 14,
};

const mediumButton = StyleSheet.create({
  buttonViewStyle: {
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  buttonTextStyle: {
    fontSize: 14,
    // letterSpacing: 1,
  },
});

export {largeButton, mediumButton, smallButton, cardButton};
