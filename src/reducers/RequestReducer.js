import {ADD_REQUEST, ALL_REQUESTS} from '../actions/Types';

const initialstate = {
  request: [],
};

export default function(state = initialstate, action) {
  switch (action.type) {
    case ADD_REQUEST:
      return {...state, request: state.request.concat(action.payload)};
    case ALL_REQUESTS:
      return {type: ALL_REQUESTS};
    default:
      return state;
  }
}
