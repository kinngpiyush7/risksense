describe('Test for Scorecard in Dashboard', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Checking Globally Benchmark Rank Card and its elements', async () => {
    await expect(element(by.id('globalRankCard'))).toBeVisible();
    await expect(element(by.id('companyLogo'))).toBeVisible();
    await expect(element(by.id('companyRank'))).toBeVisible();
    await expect(element(by.id('totalRank'))).toBeVisible();
    await expect(element(by.id('globalRank'))).toBeVisible();
    await expect(element(by.id('globalRank'))).toHaveText(
      'Globally Benchmark Rank',
    );
    await expect(element(by.id('securityPosture'))).toBeVisible();
    await expect(element(by.id('securityPosture'))).toHaveText(
      'Security Posture',
    );
    await expect(element(by.id('securityPostureValue'))).toBeVisible();
    await expect(element(by.id('healthcareIndustry'))).toBeVisible();
    await expect(element(by.id('healthcareIndustry'))).toHaveText(
      'Healthcare Industry',
    );
    await expect(element(by.id('healthcareIndustryValue'))).toBeVisible();
    await expect(element(by.id('company'))).toBeVisible();
    await expect(element(by.id('company'))).toHaveText('Company');
    await expect(element(by.id('companyValue'))).toBeVisible();
    await expect(element(by.id('domain'))).toBeVisible();
    await expect(element(by.id('domain'))).toHaveText('Domain');
    await expect(element(by.id('domainValue'))).toBeVisible();
    await expect(element(by.id('industry'))).toBeVisible();
    await expect(element(by.id('industry'))).toHaveText('Industry');
    await expect(element(by.id('industryValue'))).toBeVisible();
    await expect(element(by.id('strength'))).toBeVisible();
    await expect(element(by.id('strength'))).toHaveText('Strength');
    await expect(element(by.id('strengthValue'))).toBeVisible();
  });
});
