import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';

const CustomButton = props => {
  return (
    // Making the Reusable Button Clickable - Eli
    <TouchableOpacity
      // Using the OnPress and redirecting to a screen/function passed from the Parent Component - Eli
      onPress={() => props.handleButtonPress()}
      style={props.touchableStyle ? props.touchableStyle : null}>
      <View
        // Using the TestID Provided by the Parent Component for Testing - Eli
        testID={props.buttonTest}
        // Using styles provided by the parent component or using custom default styles - Eli
        style={[
          styles.button,
          props.buttonBackgroundColor
            ? styles.primaryBackgroundColor
            : styles.secondaryBackgroundColor,
          // props.isFullWidth && styles.fullWidth,
          props.viewStyle ? props.viewStyle : null,
          props.hasShadow ? styles.buttonShadow : null,
        ]}>
        <Text
          // Using styles provided by the parent component or using custom default styles - Eli
          style={[
            styles.fontSize16,
            styles.textCenter,
            props.textColor ? styles.textColorWhite : styles.textColorBlack,
            props.textStyle ? props.textStyle : null,
          ]}>
          {/* The text which will appear on the button is accessed via the children property - Eli */}
          {props.children}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

// The default styles applied on the button if the properties are not passed - Eli
const styles = StyleSheet.create({
  primaryBackgroundColor: {
    backgroundColor: 'rgb(255,255,255)',
  },
  fullWidth: {
    width: Dimensions.get('window').width,
  },
  secondaryBackgroundColor: {
    backgroundColor: '#565656',
  },
  textColorWhite: {
    color: '#FFF',
  },
  textColorBlack: {
    color: 'rgb(43,43,43)',
  },
  fontSize16: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
  },
  button: {
    borderRadius: 4,
  },
  textCenter: {
    textAlign: 'center',
  },
  buttonShadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
});

export default CustomButton;
