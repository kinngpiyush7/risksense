describe('Test for Cyber Risk Trend Card in Dashboard', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Cyber Risk Trend Card', async () => {
    await expect(element(by.id('cyberRiskTrendCard'))).toBeVisible();
    await expect(element(by.id('cyberRiskTrend'))).toBeVisible();
    await expect(element(by.id('cyberRiskTrend'))).toHaveText(
      'Cyber Risk Trend',
    );
    await expect(element(by.id('radarGraph'))).toBeVisible();
    await expect(element(by.id('criticalIssues'))).toBeVisible();
    await expect(element(by.id('criticalIssues'))).toHaveText(
      'Critical Issues',
    );
    await expect(element(by.id('criticalIssuesValue'))).toBeVisible();
    await expect(element(by.id('mediumIssues'))).toBeVisible();
    await expect(element(by.id('mediumIssues'))).toHaveText('Critical Issues');
    await expect(element(by.id('mediumIssuesValue'))).toBeVisible();
  });
});
