/* eslint-disable no-undef */
describe('Onboarding Feature Testing', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });
  // it('The SplashScreen should be visible and should be Clicked  ', async () => {
  //   await expect(element(by.id('splash_intro'))).toBeVisible();
  //   await element(by.id('splash')).tap();
  //   await waitFor(element(by.id('demo')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  // });

  // it('The Lets Start Screen should be tested along with included Reuseable Components', async () => {
  //   await waitFor(element(by.id('demo')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await expect(element(by.id('start_image'))).toBeVisible();
  //   await expect(element(by.id('startId'))).toBeVisible();
  //   await expect(element(by.id('startButton'))).toBeVisible();
  //   await element(by.id('startButton')).tap();
  // });

  // it('The Request Screen should be tested along with included Reuseable Components', async () => {
  //   const fname = 'Eliyahoo';
  //   const lname = 'Awaskar';
  //   const c_email = 'headstrait@gmail.com';
  //   const c_url = 'headstrait.com';
  //   await waitFor(element(by.id('demo')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await element(by.id('startButton')).tap();
  //   await expect(element(by.id('request'))).toBeVisible();
  //   await expect(element(by.id('request_screen'))).toBeVisible();
  //   await element(by.id('input_name')).tap();
  //   await waitFor(element(by.id('input_name')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await element(by.id('input_name')).typeText(fname);
  //   await element(by.id('input_lname')).tap();
  //   await waitFor(element(by.id('input_lname')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await element(by.id('input_lname')).typeText(lname);
  //   await element(by.id('input_email')).tap();
  //   await waitFor(element(by.id('input_email')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await element(by.id('input_email')).typeText(c_email);
  //   await device.pressBack();
  //   await waitFor(element(by.id('input_url')))
  //     .toBeVisible()
  //     .withTimeout(10000);
  //   await element(by.id('input_url')).tap();
  //   await waitFor(element(by.id('input_url')))
  //     .toBeVisible()
  //     .withTimeout(30000);
  //   await device.pressBack();
  //   // await element(by.id('input_url')).typeText(c_url);
  //   await element(by.id('requestButton')).tap();
  // });

  it('The Thank You Screen should be tested along with included Reuseable Components', async () => {
    const fname = 'Eliyahoo';
    const lname = 'Awaskar';
    const c_email = 'headstrait@gmail.com';
    const c_url = 'headstrait.com';
    await waitFor(element(by.id('demo')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('startButton')).tap();
    await element(by.id('input_name')).tap();
    await waitFor(element(by.id('input_name')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_name')).typeText(fname);
    await element(by.id('input_lname')).tap();
    await waitFor(element(by.id('input_lname')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_lname')).typeText(lname);
    await element(by.id('input_email')).tap();
    await waitFor(element(by.id('input_email')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_email')).typeText(c_email);
    await device.pressBack();
    await waitFor(element(by.id('input_url')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('input_url')).tap();
    await waitFor(element(by.id('input_url')))
      .toBeVisible()
      .withTimeout(30000);
    await device.pressBack();
    await waitFor(element(by.id('requestButton')))
      .toBeVisible()
      .withTimeout(30000);
    await element(by.id('requestButton')).tap();
    // await waitFor(element(by.id('demo2')))
    //   .toBeVisible()
    //   .withTimeout(10000);
    // await expect(element(by.id('demo2'))).toBeVisible();
    // await waitFor(element(by.id('thankyou_screen')))
    //   .toBeVisible()
    //   .withTimeout(10000);
    // await expect(element(by.id('thankyou_button'))).toBeVisible();
    // await element(by.id('thankyou_button')).tap();
  });
});
