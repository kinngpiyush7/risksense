import React, {Component} from 'react';
import {View, TouchableOpacity, Text, Image, TextInput} from 'react-native';
import arrow from '../assets/images/arw.png';

export default class Pagination extends Component {
  state = {
    pageNumbers: [],
    currentpage: 1,
    maxpage: ' ',
    minpage: 1,
  };

  onPress = async () => {
    if (this.state.currentpage !== this.state.maxpage) {
      await this.setState({currentpage: this.state.currentpage + 1});
    }
    this.props.handlePageChange(this.state.currentpage);
  };
  onPressBack = async () => {
    if (this.state.currentpage !== this.state.minpage) {
      await this.setState({currentpage: this.state.currentpage - 1});
    }
    this.props.handlePageChange(this.state.currentpage);
  };
  componentDidMount() {
    let pageNumbers = [];
    //  CREATED - Dividing the total pages with the per page
    // example 50/10 = 5 pages, then pushing the count to an empty array declared above

    for (
      let i = 1;
      i <= Math.ceil(this.props.totalRows / this.props.RowPerPage);
      i++
    ) {
      pageNumbers.push(i);
    }
    this.setState({maxpage: pageNumbers.length});
  }

  render() {
    return (
      <View
        testID="pagination"
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-end',
          marginTop: 10,
        }}>
        {/*  - OnClick of each page the page number will be updated
            by the handlePage method with the current page number will change*/}

        <View style={{flexDirection: 'row', color: 'rgb(86,86,86)'}}>
          <Text
            style={{
              fontFamily: 'Poppins-Regular',
              color: 'rgb(86,86,86)',
              fontSize: 13,
            }}>
            1 - {this.state.maxpage} of 67 Result found{'  '}
          </Text>
          <TouchableOpacity onPress={this.onPressBack}>
            <Image
              style={{
                height: 10,
                width: 8,
                marginTop: 5,
                marginRight: 5,
                transform: [{rotate: '180deg'}],
              }}
              source={arrow}></Image>
          </TouchableOpacity>
          <TextInput
            style={{
              backgroundColor: 'white',
              height: 20,
              width: 20,
              padding: 0,
              paddingLeft: 6,
              borderColor: 'rgb(207,207,207)',
              borderWidth: 1,
              color: 'black',
              fontSize: 13,
            }}
            value={`${this.state.currentpage}`}
          />

          <Text
            style={{
              color: 'rgb(86,86,86)',
              fontSize: 13,
              fontFamily: 'Poppins-Regular',
            }}>
            {' '}
            of {this.state.maxpage}
          </Text>
        </View>
        <View>
          <TouchableOpacity onPress={this.onPress}>
            <Image
              style={{height: 10, width: 8, marginTop: 5, marginLeft: 5}}
              source={arrow}></Image>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
