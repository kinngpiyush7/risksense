import React, {Component} from 'react';
import {View, StyleSheet, ImageBackground, Text, Keyboard} from 'react-native';
import CustomButton from '../components/CustomButton';
import CustomOnboardingScreen from '../components/CustomOnboardingScreen';
import FloatingLabelInput from '../components/FloatingLabelInput';
import {SafeAreaView} from 'react-navigation';

import {largeButton} from '../assets/styles/Buttons';

export default class CheckDomainScreen extends Component {
  static navigationOptions = {header: null};
  state = {
    new_email: '',
    new_url: '',
  };

  handleTextChange = (n1, n2) =>
    this.setState({
      new_email: n1,
      new_url: n2,
    });

  Redirect = () => {
    this.props.navigation.navigate('thankYouScreen');
  };

  render() {
    return (
      <SafeAreaView testID="domain" onPress={Keyboard.dismiss}>
        <ImageBackground
          source={require('../assets/images/LoadingScreen.png')}
          style={styles.backgroundImage}>
          <CustomOnboardingScreen
            onboardingTest="domain_screen"
            header={<Text>Request a Free {'\n'}Cyber Risk Scorecard</Text>}
            headerFontSize={22}
            imageStyle={{marginLeft: 50, marginTop: 42}}
            main2={true}
            mainDescStyle={{
              paddingVertical: 28,
              paddingLeft: 50,
              paddingRight: 80,
            }}
            inputField={true}>
            <View style={styles.inputs}>
              <View>
                <FloatingLabelInput
                  inputTest="comp_email"
                  label="Company Email"
                  isFocused={true}
                  value={this.state.new_email}
                  onChangeText={text => this.setState({new_email: text})}
                  blurOnSubmit
                  testID="new_email"
                />
                <FloatingLabelInput
                  label="Company Website URL"
                  isFocused={true}
                  value={this.state.new_url}
                  onChangeText={text2 => this.setState({new_url: text2})}
                  blurOnSubmit
                  testID="new_url"
                />
              </View>

              <View>
                <View style={styles.oneButton}>
                  <CustomButton
                    buttonTest="domain_button"
                    buttonBackgroundColor={true}
                    textColor={false}
                    hasShadow={true}
                    isFullWidth={false}
                    viewStyle={[largeButton.buttonViewStyle]}
                    textStyle={largeButton.buttonTextStyle}
                    handleButtonPress={this.Redirect}>
                    Submit Request
                  </CustomButton>
                </View>
              </View>
            </View>
            <View style={styles.terms}>
              <Text style={styles.terms_font}>Terms and Conditions</Text>
            </View>
          </CustomOnboardingScreen>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },

  startHeight: {
    height: 128,
  },

  requestWidth: {
    width: '70%',
  },
  inputs: {
    flex: 1,
    justifyContent: 'space-between',
  },
  oneButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  terms: {
    marginTop: 30,
    marginBottom: 20,
    alignItems: 'center',
  },
  terms_font: {
    color: 'rgb(155,155,155)',
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
});
