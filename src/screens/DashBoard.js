import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {SafeAreaView} from 'react-navigation';

import ReconnaissanceOverview from '../components/ReconnaissanceOverview';
import GlobalRanking from '../components/GlobalRanking';
import TopFixes from '../components/TopFixes';
import CyberRiskTrend from '../components/CyberRiskTrend';
import Footer from '../components/Footer';
import OrganizationalScoresCard from '../components/OrganizationalScores/OrganizationalScoresCard';
import ApplicationSecurityCard from '../components/OrganizationalScores/ApplicationSecurityCard';
import PatchingCadenceCard from '../components/OrganizationalScores/PatchingCadenceCard';
import NetworkSecurityCard from '../components/OrganizationalScores/NetworkSecurityCard';
import EmailSecurityCard from '../components/OrganizationalScores/EmailSecurityCard';
import DNSSecurityCard from '../components/OrganizationalScores/DNSSecurityCard';

export default class DashBoard extends Component {
  static navigationOptions = {
    header: null,
  };
  state = {
    date: '',
    activeScreen: '',
    showCard: false,
  };

  componentDidMount() {
    var that = this;
    var date = new Date().getDate();
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    const month = monthNames[new Date().getMonth()];
    var year = new Date().getFullYear();
    that.setState({
      date: date + ' ' + month + ' ' + year,
    });
  }

  changeActiveCard = async screen => {
    await this.setState({activeScreen: screen});
    if (
      this.state.activeScreen === '' ||
      this.state.activeScreen === 'IP Reputation'
    ) {
      this.setState({showCard: false});
    } else {
      this.setState({showCard: true});
    }
  };

  getOrgCard = () => {
    if (this.state.activeScreen === 'Application Security') {
      return (
        <ApplicationSecurityCard
          navigate={this.props.navigation.navigate}
          activeScreen={screen => this.changeActiveCard(screen)}
        />
      );
    } else if (this.state.activeScreen === 'Patching Cadence') {
      return (
        <PatchingCadenceCard
          navigate={this.props.navigation.navigate}
          activeScreen={screen => this.changeActiveCard(screen)}
        />
      );
    } else if (this.state.activeScreen === 'Network Security') {
      return (
        <NetworkSecurityCard
          navigate={this.props.navigation.navigate}
          activeScreen={screen => this.changeActiveCard(screen)}
        />
      );
    } else if (this.state.activeScreen === 'Email Security') {
      return (
        <EmailSecurityCard
          navigate={this.props.navigation.navigate}
          activeScreen={screen => this.changeActiveCard(screen)}
        />
      );
    } else if (this.state.activeScreen === 'DNS Security') {
      return (
        <DNSSecurityCard
          navigate={this.props.navigation.navigate}
          activeScreen={screen => this.changeActiveCard(screen)}
        />
      );
    } else {
      return (
        <>
          <GlobalRanking />
          <View style={{height: 16}}></View>
          <CyberRiskTrend />
        </>
      );
    }
  };

  render() {
    return (
      <SafeAreaView>
        {/* <StatusBar /> */}
        <ScrollView>
          <View style={{padding: 16}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                marginBottom: 30,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Image
                  testID="bigCompanyLogo"
                  source={require('../assets/images/merck.png')}
                  style={styles.companyLogo}
                />
                <Text testID="compnanyDomainName" style={styles.domain}>
                  www.merck.com
                </Text>
                <Text style={styles.partition}>|</Text>
                <Text testID="currentDate" style={styles.date}>
                  {this.state.date}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <TouchableOpacity
                  testID="checkNewDomain"
                  style={{marginRight: 15}}
                  onPress={() => {
                    this.props.navigation.navigate('checkNewDomain');
                  }}>
                  <Text style={styles.domain}>Check New Domain?</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    borderRadius: 4,
                    backgroundColor: 'rgb(86,86,86)',
                    paddingLeft: 15,
                    paddingRight: 15,
                    paddingTop: 10,
                    paddingBottom: 10,
                    shadowColor: '#000',
                    shadowOffset: {
                      width: 0,
                      height: 3,
                    },
                    shadowOpacity: 0.27,
                    shadowRadius: 4.65,
                    elevation: 6,
                  }}
                  onPress={() =>
                    this.props.navigation.navigate('SubscriptionPlan')
                  }>
                  <Text
                    testID="deeperScanReportButton"
                    style={{
                      fontSize: 14,
                      color: 'white',
                      fontFamily: 'Poppins-Medium',
                    }}>
                    Get a Deeper Scan Report
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            {/* ====================================================================== */}
            <View style={{flexDirection: 'row'}}>
              <View style={styles.firstContainer}>
                <ReconnaissanceOverview
                  navigation={this.props.navigation.navigate}
                />
                <TopFixes />
              </View>
              <View style={styles.secondContainer}>
                <View
                  style={[
                    styles.topContainer,
                    {height: this.state.showCard ? 690 : null},
                  ]}>
                  <View style={{flex: 1}}>
                    <OrganizationalScoresCard
                      currentItem={this.state.activeScreen}
                      activeScreen={screen => this.changeActiveCard(screen)}
                    />
                  </View>
                  <View style={{flex: 1, marginLeft: 12}}>
                    <View>{this.getOrgCard()}</View>
                  </View>
                </View>
                {this.state.showCard ? (
                  <View style={styles.bottomContainer}>
                    <GlobalRanking />
                    <View style={{width: 24}}></View>
                    <CyberRiskTrend />
                  </View>
                ) : null}
              </View>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  firstContainer: {
    flex: 1,
    marginRight: 12,
  },
  secondContainer: {
    flex: 2,
    marginLeft: 12,
    flexWrap: 'wrap',
  },
  topContainer: {
    flex: 1,
    flexDirection: 'row',
    width: '100%',
  },
  bottomContainer: {
    width: '100%',
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  domain: {
    color: 'rgb(86,86,86)',
    fontSize: 14,
    marginLeft: 15,
    fontFamily: 'Poppins-SemiBold',
  },
  date: {
    color: 'rgb(86,86,86)',
    fontSize: 14,
    fontFamily: 'Poppins-SemiBold',
  },
  partition: {
    color: 'rgb(86,86,86)',
    fontSize: 16,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 2,
  },
  companyLogo: {
    width: 130,
    height: 40,
  },
});
