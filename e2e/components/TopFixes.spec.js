describe('Test for Top Fixes & Categories card in Dashboard', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('Top Fixes & Categories Card', async () => {
    await expect(element(by.id('topFixesCard'))).toBeVisible();
    await expect(element(by.id('topFixes'))).toBeVisible();
    await expect(element(by.id('topFixes'))).toHaveText(
      'Top Fixes & Categories',
    );
    await expect(element(by.id('circleCard'))).toBeVisible();
    await expect(element(by.id('problemsFoundValue'))).toBeVisible();
    await expect(element(by.id('problems'))).toBeVisible();
    await expect(element(by.id('problems'))).toHaveText('problems');
    await expect(element(by.id('found'))).toBeVisible();
    await expect(element(by.id('found'))).toHaveText('found');
    await expect(element(by.id('vulnerabilityValue'))).toBeVisible();
    await expect(element(by.id('vulnerability'))).toBeVisible();
    await expect(element(by.id('vulnerability'))).toHaveText(
      'Vulnerability Related',
    );
    await expect(element(by.id('config'))).toBeVisible();
    await expect(element(by.id('config'))).toHaveText('Configuration Related');
    await expect(element(by.id('configValue'))).toBeVisible();
    await expect(element(by.id('topActions'))).toBeVisible();
    await expect(element(by.id('topActions'))).toHaveText(
      'Top Remediation Actions',
    );
    await expect(element(by.id('spfPractices'))).toBeVisible();
    await expect(element(by.id('spfPractices'))).toHaveText(
      'SPF Best Practises not followed',
    );
    await expect(element(by.id('spfPracticesValue'))).toBeVisible();
    await expect(element(by.id('patchingCadence'))).toBeVisible();
    await expect(element(by.id('patchingCadence'))).toHaveText(
      'Patching Cadence for CVE-2014-4078',
    );
    await expect(element(by.id('patchingCadenceValue'))).toBeVisible();
    await expect(element(by.id('vulnarability'))).toBeVisible();
    await expect(element(by.id('vulnarability'))).toHaveText(
      'Vulnerability: CVE 2014-3456',
    );
    await expect(element(by.id('vulnarabilityValue'))).toBeVisible();
    await expect(element(by.id('dnsecEntry'))).toBeVisible();
    await expect(element(by.id('dnsecEntry'))).toHaveText(
      'DNSEC Entry Missing for Domain',
    );
    await expect(element(by.id('dnsecEntryValue'))).toBeVisible();
    await expect(element(by.id('openPort443'))).toBeVisible();
    await expect(element(by.id('openPort443'))).toHaveText(
      'Open Port 443 is detected',
    );
    await expect(element(by.id('openPort443Value'))).toBeVisible();
    await expect(element(by.id('sdaPractices'))).toBeVisible();
    await expect(element(by.id('sdaPractices'))).toHaveText(
      'SDA Best Practises not followed',
    );
    await expect(element(by.id('sdaPracticesValue'))).toBeVisible();
    await expect(element(by.id('openPort8907'))).toBeVisible();
    await expect(element(by.id('openPort8907'))).toHaveText(
      'Open Port 8907 is detected',
    );
    await expect(element(by.id('openPort8907Value'))).toBeVisible();
  });
});
