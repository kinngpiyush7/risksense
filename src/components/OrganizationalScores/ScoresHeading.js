import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const ScoresHeading = props => {
  return (
    <View style={styles.headingBackground}>
      <Text testID={props.headingTextId} style={styles.headingText}>
        {props.children}
      </Text>
    </View>
  );
};

export default ScoresHeading;

const styles = StyleSheet.create({
  headingBackground: {
    backgroundColor: 'rgb(198,18,39)',
    padding: 12,
    borderRadius: 2,
  },
  headingText: {
    color: '#ffffff',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Poppins-SemiBold',
  },
});
