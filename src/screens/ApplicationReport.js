import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import ReportTable from '../components/ReportTable';
import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import arrow from '../assets/images/triangle.png';

import ApplicationCircle from '../components/ApplicationCircle';
import {SafeAreaView} from 'react-navigation';
import {widthPercentageToDP} from '../components/Responsive';

import CustomButton from '../components/CustomButton';
import {mediumButton} from '../assets/styles/Buttons';
import Tag from '../components/OrganizationalScores/Tag';
import App_image from '../assets/images/appSecurity/srsIconsAppSecurity.png';

export default class ApplicationReport extends Component {
  state = {CurrentPage: 1, RowPerPage: 10};
  handlePageChange = pageNumber => {
    this.setState({CurrentPage: pageNumber});
  };
  static navigationOptions = {
    header: null,
  };

  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };
  render() {
    // data for table
    const data = [
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#A31727',
                  borderColor: '#A31727',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#FE8F3B',
                  borderColor: '#FE8F3B',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#0FB7C7',
                borderColor: '#0FB7C7',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SSL Certificate Provides Extended Validation
        </Text>,
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#0FB7C7',
                borderColor: '#0FB7C7',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#F2B924',
                borderColor: '#F2B924',
              }}
            />
          </View>
        </View>,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SSL Certificate Validity Too Long
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#C61227',
                  borderColor: '#C61227',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#C61227',
                  borderColor: '#C61227',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#0FB7C7',
                borderColor: '#0FB7C7',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-10'}
          customText={{fontSize: 10}}
          customStyle={{
            width: 60,
            backgroundColor: '#F2B924',
            borderColor: '#F2B924',
          }}
        />,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SNMP Agent Defualt communication
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#FF8930',
                borderColor: '#FF8930',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#9E0A1B',
                borderColor: '#9E0A1B',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#9E0A1B',
                  borderColor: '#9E0A1B',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#FF8930',
                  borderColor: '#FF8930',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SNMP Agent Defualt communication
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#F3BE35',
                borderColor: '#F3BE35',
              }}
            />
          </View>
        </View>,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-10'}
          customText={{fontSize: 10}}
          customStyle={{
            width: 60,
            backgroundColor: '#FF8930',
            borderColor: '#FF8930',
          }}
        />,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#FF8930',
                  borderColor: '#FF8930',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#F3BE35',
                  borderColor: '#F3BE35',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#0FB7C7',
                borderColor: '#0FB7C7',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SSL Certificate Provides Extended Validation
        </Text>,
        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#F3BE35',
                borderColor: '#F3BE35',
              }}
            />
          </View>
        </View>,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SSL Certificate Validity Too Long
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#02B4C4',
                  borderColor: '#02B4C4',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#F3BE35',
                  borderColor: '#F3BE35',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <Tag
          tag={'CVE-10'}
          customText={{fontSize: 10}}
          customStyle={{
            width: 60,
            backgroundColor: '#F3BE35',
            borderColor: '#F3BE35',
          }}
        />,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SNMP Agent Defualt communication
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#F3BE35',
                borderColor: '#F3BE35',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +1.24 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View>
          <View style={{flexDirection: 'row'}}>
            <View style={{marginRight: 4}}>
              <Tag
                tag={'CVE-10'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#F3BE35',
                  borderColor: '#F3BE35',
                }}
              />
            </View>
            <View>
              <Tag
                tag={'CWE-213'}
                customText={{fontSize: 10}}
                customStyle={{
                  width: 60,
                  backgroundColor: '#F3BE35',
                  borderColor: '#F3BE35',
                }}
              />
            </View>
          </View>
          <View style={{marginTop: 2}}>
            <Tag
              tag={'APPSEC-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 70,
                backgroundColor: '#F3BE35',
                borderColor: '#F3BE35',
              }}
            />
          </View>
        </View>,
        <Text style={{fontFamily: 'Poppins-Medium'}}>
          SNMP Agent Defualt communication
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +34.09 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
      [
        <View style={{flexDirection: 'row'}}>
          <View style={{marginRight: 4}}>
            <Tag
              tag={'CVE-10'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#F3BE35',
                borderColor: '#F3BE35',
              }}
            />
          </View>
          <View>
            <Tag
              tag={'CWE-213'}
              customText={{fontSize: 10}}
              customStyle={{
                width: 60,
                backgroundColor: '#02B4C4',
                borderColor: '#02B4C4',
              }}
            />
          </View>
        </View>,

        <Text style={{fontFamily: 'Poppins-Medium'}}>
          Oracle Java SE Critical
        </Text>,

        <Text style={{color: 'rgb(48,48,48)'}}>
          Cross-site request forgery (CSRF) vulnerabilies may arise when
          applications rely soley
        </Text>,
        <Text>
          +0.63 <Image style={{height: 10, width: 10}} source={arrow}></Image>
        </Text>,
      ],
    ];
    //will be use for pagination when we get data from back end
    let filtereddata = data;
    // [Pagination] Fetching the first and last companies index
    // and slicing to get current companies
    let indexOfLastRow = this.state.CurrentPage * this.state.RowPerPage;
    let indexOfFirstRow = indexOfLastRow - this.state.RowPerPage;
    let current_data =
      filtereddata !== null
        ? filtereddata.slice(indexOfFirstRow, indexOfLastRow)
        : null;
    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="App_container" style={styles.App_container}>
            <View testID="App_header" style={styles.header}>
              <TouchableOpacity
                testID="App_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="App_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="App_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={App_image}></Image>
              <View testID="App_header_texts" style={styles.header_texts}>
                <Text testID="App_heading" style={styles.heading}>
                  Application Security Report
                </Text>
                <Text testID="App_sub_heading" style={styles.sub_heading}>
                  Prevent from service attacks, cyberattacks and data breaches
                  or data theft situations
                </Text>
              </View>
            </View>
            <View testID="App_graphcontainer" style={styles.App_graphcontainer}>
              {/* graphs view */}

              <View
                style={{
                  marginRight: 46,
                }}>
                <ApplicationCircle
                  maxValue="1765"
                  value1="1400"
                  value2="890"
                  value3="600"
                  small={false}
                  positiveIncrease={false}
                />
              </View>
              <View style={{marginRight: 30, width: 200}}>
                <Text style={{fontFamily: 'Poppins-SemiBold', fontSize: 14}}>
                  Total Vulnerabilities
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 16,
                    marginBottom: 13,
                  }}>
                  <Text style={{fontFamily: 'Poppins-Medium', fontSize: 14}}>
                    Critical
                  </Text>
                  <Text
                    style={{
                      marginLeft: 'auto',
                      fontFamily: 'Poppins-SemiBold',
                      fontSize: 14,
                      color: '#9E0A1B',
                    }}>
                    47
                  </Text>
                </View>

                <View style={{flexDirection: 'row', marginBottom: 13}}>
                  <Text style={{fontFamily: 'Poppins-Medium', fontSize: 14}}>
                    High
                  </Text>
                  <Text
                    style={{
                      marginLeft: 'auto',
                      fontFamily: 'Poppins-SemiBold',
                      fontSize: 14,
                      color: '#C61227',
                    }}>
                    123
                  </Text>
                </View>
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontFamily: 'Poppins-Medium', fontSize: 14}}>
                    Medium
                  </Text>
                  <Text
                    style={{
                      marginLeft: 'auto',
                      fontFamily: 'Poppins-SemiBold',
                      fontSize: 14,
                      color: '#FF8930',
                    }}>
                    23
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View testID="App_table" style={styles.App_table}>
            <ReportTable
              showLegend={true}
              heading="The application security component represents the suseptibility of target web applications to defacements and attacks resulting from outdated aplication software packages in use."
              column_names={[
                'Control ID',
                'Vulnerability',
                'Description',
                'Score Impact',
              ]}
              widthArr={[
                widthPercentageToDP('18'),
                widthPercentageToDP('30'),
                widthPercentageToDP('35'),
                widthPercentageToDP('10'),
              ]}
              column_data={current_data}
              // passing props required value for pagination
              RowPerPage={this.state.RowPerPage}
              totalRows={filtereddata.length}
              handlePageChange={this.handlePageChange}
              CurrentPage={this.state.CurrentPage}
            />
          </View>
          <View
            testID="App_bottomcontainer"
            style={{
              marginTop: 20,
              marginBottom: 30,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              testID="App_bottomcontainer_text"
              style={{flexShrink: 1, width: 400}}>
              <Text
                testID="App_bottomcontainer_text_1"
                style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                Action items to improve the Organization Score -
                <Text
                  testID="App_bottomcontainer_text_2"
                  style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                  Disable critical services like Redis, MySQL, and RDP
                </Text>
              </Text>
            </View>
            <View style={{marginLeft: 20}}>
              <CustomButton
                buttonTest="startButton"
                buttonBackgroundColor={false}
                textColor={true}
                hasShadow={true}
                navigation={this.props.navigation.navigate}
                handleButtonPress={this.handleNavigation}
                textStyle={mediumButton.buttonTextStyle}
                viewStyle={mediumButton.buttonViewStyle}
                handleButtonPress={this.navigation}>
                Get a Deeper Scan Report
              </CustomButton>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  App_container: {
    paddingHorizontal: 15,
    paddingTop: 15,
    // height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    // fontWeight: '900',
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  App_graphcontainer: {
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 20,
    // flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 6,
    // marginTop: 6,
    marginLeft: 20,
    // marginHorizontal: 14,
  },
});
