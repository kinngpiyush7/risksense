import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import ReportTable from '../components/ReportTable';
import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import LineGraph from '../components/LineGraph';
import CustomButton from '../components/CustomButton';
import Tag from '../components/OrganizationalScores/Tag';
import NR_image from '../assets/images/netSecurity/srsIconsNetworkSecurity.png';
import {mediumButton} from '../assets/styles/Buttons';
import {widthPercentageToDP} from '../components/Responsive';
import arrow from '../assets/images/down-button.png';
import {SafeAreaView} from 'react-navigation';

export default class NetworkReport extends Component {
  state = {CurrentPage: 1, RowPerPage: 10};
  handlePageChange = pageNumber => {
    this.setState({CurrentPage: pageNumber});
  };

  static navigationOptions = {
    header: null,
  };

  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };
  render() {
    // data for table
    const data = [
      [
        <Tag
          tag={10}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: '#9E0A1B',
            borderColor: '#9E0A1B',
          }}
        />,
        'SNMP Agent Default communication',
        'autodiscover.group',
        'Regstered Domain',
        '192.185.238.86',
        '3306',
      ],
      [
        <Tag
          tag={21}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(255,137,48)',
            borderColor: 'rgb(255,137,48)',
          }}
        />,
        'Red Hat Update for dns',
        'fha.merack.gov',
        'Subdomain',
        '192.185.238.86',
        '125',
      ],
      [
        <Tag
          tag={6}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(0,179,196)',
            borderColor: 'rgb(0,179,196)',
          }}
        />,
        'Oracle Java SE critical',
        'care2care.dhmh.merck.',
        'Subdomain',
        '192.185.238.86',
        '445',
      ],
      [
        <Tag
          tag={23}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(213,62,79)',
            borderColor: 'rgb(213,62,79)',
          }}
        />,
        'SNMP Agent Default communication',
        'autodiscover.group',
        'Domain',
        '192.185.238.86',
        '5900',
      ],
      [
        <Tag
          tag={24}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: '#9E0A1B',
            borderColor: '#9E0A1B',
          }}
        />,
        'Red Hat Update for dns',
        'care2care.dhmh.merck.',
        'Regstered Domain',
        '192.185.238.86',
        '3306',
      ],
      [
        <Tag
          tag={15}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(0,179,196)',
            borderColor: 'rgb(0,179,196)',
          }}
        />,
        'Oracle Java SE critical',
        'fha.merack.gov',
        'Subdomain',
        '192.185.238.86',
        '125',
      ],
      [
        <Tag
          tag={8}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(213,62,79)',
            borderColor: 'rgb(213,62,79)',
          }}
        />,
        'SNMP Agent Default communication',
        'care2care.dhmh.merck.',
        'Subdomain',
        '192.185.238.86',
        '445',
      ],
      [
        <Tag
          tag={4}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(251,206,63)',
            borderColor: 'rgb(251,206,63)',
          }}
        />,
        'Red Hat Update for dns',
        'autodiscover.group',
        'Domain',
        '192.185.238.86',
        '5900',
      ],
      [
        <Tag
          tag={15}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(213,62,79)',
            borderColor: 'rgb(213,62,79)',
          }}
        />,
        'Oracle Java SE critical',
        'care2care.dhmh.merck.',
        'NS',
        '192.185.238.86',
        '5900',
      ],
      [
        <Tag
          tag={10}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(251,206,63)',
            borderColor: 'rgb(251,206,63)',
          }}
        />,
        'SNMP Agent Default communication',
        'autodiscover.group',
        'Registered Domain',
        '192.185.238.86',
        '3306',
      ],
      [
        <Tag
          tag={21}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(0,179,196)',
            borderColor: 'rgb(0,179,196)',
          }}
        />,
        'Red Hat Update for dns',
        'fha.merck.gov',
        'Subdomain',
        '192.185.238.86',
        '125',
      ],
      [
        <Tag
          tag={6}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(251,206,63)',
            borderColor: 'rgb(251,206,63)',
          }}
        />,
        'Oracle Java SE critical',
        'care2care.dhmh.merck.',
        'Subdomain',
        '192.185.238.86',
        '445',
      ],
      [
        <Tag
          tag={23}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: '#9E0A1B',
            borderColor: '#9E0A1B',
          }}
        />,
        'SNMP Agent Default communication',
        'autodiscover.group',
        'Domain',
        '192.185.238.86',
        '5900',
      ],
      [
        <Tag
          tag={24}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(251,206,63)',
            borderColor: 'rgb(251,206,63)',
          }}
        />,
        'Red Hat Update for dns',
        'care2care.dhmh.merck.',
        'Registered Domain',
        '192.185.238.86',
        '3306',
      ],
      [
        <Tag
          tag={6}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(0,179,196)',
            borderColor: 'rgb(0,179,196)',
          }}
        />,
        'Oracle Java SE critical',
        'fha.merck.gov',
        'Subdomain',
        '192.185.238.86',
        '125',
      ],
      [
        <Tag
          tag={8}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: '#9E0A1B',
            borderColor: '#9E0A1B',
          }}
        />,
        'SNMP Agent Default communication',
        'care2care.dhmh.merck.',
        'Subdomain',
        '192.185.238.86',
        '445',
      ],
      [
        <Tag
          tag={4}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(251,206,63)',
            borderColor: 'rgb(251,206,63)',
          }}
        />,
        'Red Hat Update for dns',
        'autodiscover.group',
        'Domain',
        '192.185.238.86',
        '5900',
      ],
      [
        <Tag
          tag={6}
          customText={{fontSize: 13}}
          customStyle={{
            marginRight: 4,
            width: 50,
            backgroundColor: 'rgb(0,179,196)',
            borderColor: 'rgb(0,179,196)',
          }}
        />,
        'Oracle Java SE critical',
        'care2care.dhmh.merck.',
        'NS',
        '192.185.238.86',
        '5900',
      ],
    ];
    //will be use for pagination when we get data from back end
    let filtereddata = data;
    // [Pagination] Fetching the first and last companies index
    // and slicing to get current companies
    let indexOfLastRow = this.state.CurrentPage * this.state.RowPerPage;
    let indexOfFirstRow = indexOfLastRow - this.state.RowPerPage;
    let current_data =
      filtereddata !== null
        ? filtereddata.slice(indexOfFirstRow, indexOfLastRow)
        : null;
    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="NS_container" style={styles.NS_container}>
            <View testID="NR_header" style={styles.header}>
              <TouchableOpacity
                testID="NR_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="NR_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="PR_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={NR_image}></Image>
              <View testID="NR_header_texts" style={styles.header_texts}>
                <Text testID="NR_heading" style={styles.heading}>
                  Network Security Report
                </Text>
                <Text testID="NR_sub_heading" style={styles.sub_heading}>
                  An integration of multiple layers of defenses in the network
                  and at the network
                </Text>
              </View>
            </View>
            <View testID="NR_graphcontainer" style={styles.NR_graphcontainer}>
              <View
                testID="NR_graphcontainer_heading"
                style={{flexShrink: 1, flexBasis: '40%', width: 450}}>
                <Text
                  testID="NR_graphcontainer_heading_text"
                  style={{fontSize: 11.5, fontFamily: 'Poppins-Medium'}}>
                  Summarizes the top ports open in the target infrastructure
                  based on the number of assets on which they are open
                </Text>
              </View>
              <View
                testID="NR_linegraph"
                style={{
                  flexBasis: '60%',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginLeft: 50,
                }}>
                <View style={{flex: 1, marginLeft: 20}}>
                  <LineGraph
                    heading="Port"
                    graph_line={[
                      {port: 1369, value: 1369, text: 80, color: '#E35C6C'},
                      {port: 1369, value: 1369, text: 443, color: '#E35C6C'},
                      {port: 95, value: 670, text: 53, color: '#E35C6C'},
                      {port: 87, value: 620, text: 8080, color: '#E35C6C'},
                      {port: 76, value: 400, text: 25, color: '#E35C6C'},
                      {port: 66, value: 370, text: 8443, color: '#E35C6C'},
                      {port: 64, value: 340, text: 21, color: '#E35C6C'},
                      {port: 59, value: 310, text: 22, color: '#E35C6C'},
                      {port: 55, value: 280, text: 110, color: '#E35C6C'},
                      {port: 54, value: 250, text: 143, color: '#E35C6C'},
                    ]}
                    maxValue={700}
                    // lineColor="#E35C6C"
                  />
                </View>
                <View style={{flex: 1}}>
                  <LineGraph
                    heading="Assets"
                    graph_line={[
                      {port: 1369, value: 1369, text: 'RDP', color: '#FF8930'},
                      {
                        port: 1369,
                        value: 1369,
                        text: 'MySQl',
                        color: '#FF8930',
                      },
                      {port: 95, value: 670, text: 'SunRPC', color: '#FF8930'},
                      {port: 87, value: 620, text: 'Redis', color: '#FF8930'},
                      {port: 76, value: 400, text: 'VNC', color: '#FF8930'},
                      {port: 66, value: 370, text: 'NetBIOS', color: '#FF8930'},
                      {
                        port: 64,
                        value: 340,
                        text: 'Smart Install',
                        color: '#FF8930',
                      },
                      {port: 59, value: 310, text: 'Telnet', color: '#FF8930'},
                      {port: 55, value: 280, text: 'RDP', color: '#FF8930'},
                      {port: 54, value: 250, text: 'MySQL', color: '#FF8930'},
                    ]}
                    maxValue={700}
                    // lineColor="#FF8930"
                  />
                </View>
              </View>
            </View>
          </View>
          <View testID="NR_table" style={styles.NR_table}>
            {filtereddata.length > 0 ? (
              <ReportTable
                showLegend={true}
                heading="The network security scoring component checks for open ports and
          misconfigurtations of sevices running on them. The process involes
          analyzing publicly available asset profiles for open ports and
          services running on them."
                column_names={[
                  '  ',
                  'Vulnerability',
                  'Host Name/ Domain',
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: '#8C8C8C',
                        fontFamily: 'Poppins-Medium',
                        fontSize: 14,
                      }}>
                      Type
                    </Text>
                    <Image
                      style={{
                        marginLeft: 8,
                        marginTop: 6,
                        height: 8,
                        width: 10,
                      }}
                      source={arrow}></Image>
                  </View>,
                  <View style={{flexDirection: 'row'}}>
                    <Text
                      style={{
                        color: '#8C8C8C',
                        fontFamily: 'Poppins-Medium',
                        fontSize: 14,
                      }}>
                      IP Address
                    </Text>
                    <Image
                      style={{
                        marginLeft: 8,
                        marginTop: 6,
                        height: 8,
                        width: 10,
                      }}
                      source={arrow}></Image>
                  </View>,
                  'Port',
                ]}
                widthArr={[
                  widthPercentageToDP('9'),
                  widthPercentageToDP('25'),
                  widthPercentageToDP('20'),
                  widthPercentageToDP('15'),
                  widthPercentageToDP('15'),
                  widthPercentageToDP('10'),
                ]}
                // widthArr={[90, 300, 200, 170, 165, 35]}
                column_data={current_data}
                // passing props required value for pagination
                RowPerPage={this.state.RowPerPage}
                totalRows={filtereddata.length}
                handlePageChange={this.handlePageChange}
                CurrentPage={this.state.CurrentPage}
              />
            ) : (
              <View testID="no-result-found"> No Result Found</View>
            )}
          </View>
          <View
            testID="NR_bottomcontainer"
            style={{
              marginTop: 20,
              marginBottom: 30,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              testID="NR_bottomcontainer_text"
              style={{flexShrink: 1, width: 500}}>
              <Text
                testID="NR_bottomcontainer_text_1"
                style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                Action items to improve the Organization Score -
                <Text
                  testID="NR_bottomcontainer_text_2"
                  style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                  Disable critical services like Redis, MySQL, and RDP
                </Text>
              </Text>
            </View>
            <View style={{marginLeft: 20}}>
              <CustomButton
                buttonTest="startButton"
                buttonBackgroundColor={false}
                textColor={true}
                hasShadow={true}
                navigation={this.props.navigation.navigate}
                handleButtonPress={this.handleNavigation}
                textStyle={mediumButton.buttonTextStyle}
                viewStyle={mediumButton.buttonViewStyle}
                handleButtonPress={this.navigation}>
                Get a Deeper Scan Report
              </CustomButton>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  NS_container: {
    paddingTop: 15,
    paddingHorizontal: 15,
    // height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  NR_graphcontainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginBottom: 20,
  },
});
