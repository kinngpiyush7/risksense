import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  StyleSheet,
  TouchableHighlight,
} from 'react-native';

export default class SplashScreen extends Component {
  static navigationOptions = {header: null};
  componentDidMount() {
    // Start counting when the page is loaded
    this.timeoutHandle = setTimeout(() => {
      // Add your logic for the transition
      this.props.navigation.navigate('startScreen'); // what to push here?
    }, 1000);
  }

  render() {
    return (
      <View testID="splash_intro">
        <TouchableHighlight testID="splash">
          <ImageBackground
            source={require('../assets/images/Background_main.jpg')}
            style={styles.backgroundImage}
          />
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
});
