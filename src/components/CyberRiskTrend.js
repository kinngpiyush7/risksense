import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {WebView} from 'react-native-webview';
import {heightPercentageToDP} from '../components/Responsive';

export default class CyberRiskTrend extends Component {
  render() {
    return (
      <View style={styles.cardContainer}>
        <View testID="cyberRiskTrendCard" style={styles.cardInterior}>
          <Text testID="cyberRiskTrend" style={styles.title}>
            Cyber Risk Trend
          </Text>
          <View
            testID="radarGraph"
            style={{
              height: heightPercentageToDP('41'),

              justifyContent: 'center',
            }}>
            <WebView source={{uri: 'https://adi-shetty.github.io/'}} />
          </View>
          <View style={styles.legendContainer}>
            <View>
              <Text testID="criticalIssues" style={styles.legend}>
                Critical Issues
              </Text>
              <View
                style={[
                  styles.legendMark,
                  {backgroundColor: 'rgb(233,103,119)'},
                ]}
              />
              <Text testID="criticalIssuesValue" style={styles.legendValue}>
                35
              </Text>
            </View>
            <View style={{width: 0.8, backgroundColor: 'rgb(210,210,210)'}} />
            <View>
              <Text testID="mediumIssues" style={styles.legend}>
                Medium Issues
              </Text>
              <View
                style={[
                  styles.legendMark,
                  {backgroundColor: 'rgb(251,206,63)'},
                ]}
              />
              <Text testID="mediumIssuesValue" style={styles.legendValue}>
                27
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    flexBasis: '10%',
  },
  cardInterior: {
    padding: 15,
    borderColor: '#ededed',
    borderWidth: 2,
    borderRadius: 4,
  },
  title: {
    fontFamily: 'Poppins-SemiBold',
    color: 'rgb(43,43,43)',
    fontSize: 16,
  },
  legendContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  legend: {
    fontFamily: 'Poppins-Medium',
    color: 'rgb(74,74,74)',
    fontSize: 13.2,
  },
  legendValue: {
    fontFamily: 'Poppins-SemiBold',
    color: 'rgb(74,74,74)',
    fontSize: 16,
  },
  legendMark: {
    width: 51,
    height: 5,
    marginVertical: 5,
    borderRadius: 2.9,
  },
});
