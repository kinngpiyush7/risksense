import React, {Component} from 'react';
import {View, StyleSheet, ImageBackground, Text} from 'react-native';
import CustomButton from '../components/CustomButton';
import CustomOnboardingScreen from '../components/CustomOnboardingScreen';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-navigation';

import {getAllRequests} from '../actions/Requests';
import bgImage from '../assets/images/LoadingScreen.png';

import {largeButton} from '../assets/styles/Buttons';
import {widthPercentageToDP} from '../components/Responsive';

export class ThankYouScreen extends Component {
  static navigationOptions = {header: null};

  onRedirect = () => {
    this.props.navigation.navigate('DashBoard');
  };

  render() {
    return (
      <SafeAreaView testID="demo2">
        <ImageBackground source={bgImage} style={styles.backgroundImage}>
          <CustomOnboardingScreen
            onboardingTest="thankyou_screen"
            header="Thank You"
            headerFontSize={22}
            imageStyle={{marginLeft: 50, marginTop: 42}}
            mainDescStyle={{
              paddingVertical: 28,
              paddingLeft: 50,
              paddingRight: 80,
            }}
            inputField={true}>
            <View style={styles.thankyouContainer}>
              <Text style={styles.FirstText}>
                {`Dear ${this.props.navigation.getParam('fname')}`}
              </Text>
              <Text style={styles.SecondText}>
                Thank you for requesting a demo with RiskSense SRS, please have
                a look overview of your company's risk factors :)
              </Text>
              <View style={styles.oneButton}>
                <CustomButton
                  buttonTest="thankyou_button"
                  buttonBackgroundColor={true}
                  textColor={false}
                  hasShadow={true}
                  isFullWidth={false}
                  viewStyle={{
                    paddingVertical: 16,
                    paddingHorizontal: 16,
                    width: widthPercentageToDP('24'),
                  }}
                  textStyle={largeButton.buttonTextStyle}
                  handleButtonPress={this.onRedirect}>
                  Cyber Risk Scorecard
                </CustomButton>
              </View>
            </View>
            <View style={styles.terms}>
              <Text style={styles.terms_font}>Terms and Conditions</Text>
            </View>
          </CustomOnboardingScreen>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  thankyouContainer: {
    flex: 1,
    justifyContent: 'center',
  },

  requestWidth: {
    width: '30%',
  },

  FirstText: {
    color: '#9F9F9F',
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
  },

  SecondText: {
    fontSize: 18,
    color: '#5E5E5E',
    marginTop: 62,
    marginBottom: 50,
    letterSpacing: 0.4,
    lineHeight: 26,
    fontFamily: 'Poppins-Medium',
  },

  terms: {
    marginBottom: 20,
    alignItems: 'center',
  },
  terms_font: {
    fontFamily: 'Poppins-Medium',
    color: '#909090',
  },
});

const mapStateToProps = state => ({
  request: state.requestReducer.request,
});

export default connect(mapStateToProps, {getAllRequests})(ThankYouScreen);
