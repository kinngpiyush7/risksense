import React, {Component} from 'react';
import {View, StyleSheet, Text, ImageBackground, Keyboard} from 'react-native';
import FloatingLabelInput from '../components/FloatingLabelInput';
import CustomButton from '../components/CustomButton';
import CustomOnboardingScreen from '../components/CustomOnboardingScreen';
import {addNewRequest} from '../actions/Requests';
import {connect} from 'react-redux';
import {SafeAreaView} from 'react-navigation';

import {largeButton} from '../assets/styles/Buttons';

export class RequestScreen extends Component {
  static navigationOptions = {header: null};
  state = {
    fname: '',
    lname: '',
    c_email: '',
    c_url: 'www.',
    errors: {},
    formValid: true,
  };
  handleNavigate = () => {
    if (this.formValidation()) {
      let request = {
        fname: this.state.fname,
        lname: this.state.lname,
        c_email: this.state.c_email,
        c_url: this.state.c_url,
      };
      this.props.addNewRequest(request);
      this.setState({
        fname: '',
        lname: '',
        c_email: '',
        c_url: 'www.',
      });
      this.props.navigation.navigate('thankYouScreen', {
        fname: this.state.fname,
      });
    }
  };

  formValidation = () => {
    let errors = {};
    let formValid = true;
    if (!this.state.fname) {
      formValid = false;
      errors['fname'] = 'First Name Required';
    }
    if (!this.state.lname) {
      formValid = false;
      errors['lname'] = 'Last Name Required';
    }
    if (!this.state.c_email) {
      formValid = false;
      errors['c_email'] = 'Company Email Required';
    }
    if (!this.state.c_url || this.state.c_url.length <= 4) {
      formValid = false;
      errors['c_url'] = 'Company Website URL Required';
    }
    this.setState({
      errors: errors,
      formValid,
    });
    return formValid;
  };

  render() {
    const {errors} = this.state;
    return (
      <SafeAreaView testID="request" onPress={Keyboard.dismiss}>
        <ImageBackground
          source={require('../assets/images/LoadingScreen.png')}
          style={styles.backgroundImage}>
          <CustomOnboardingScreen
            onboardingTest="request_screen"
            header={<Text>Request a Free {'\n'}Cyber Risk Scorecard</Text>}
            headerFontSize={22}
            imageStyle={{marginLeft: 50, marginTop: 42}}
            // height={styles.startHeight}
            mainDescStyle={{
              paddingVertical: 28,
              paddingLeft: 50,
              paddingRight: 80,
            }}
            inputField={true}>
            <View style={styles.inputs}>
              <View>
                <FloatingLabelInput
                  inputTest="input_name"
                  label="First Name"
                  isFocused={true}
                  value={this.state.fname}
                  returnKeyType="next"
                  onChangeText={text => this.setState({fname: text})}
                  blurOnSubmit
                  testID="fname"
                />
                {errors.fname ? (
                  <Text style={styles.error}>{errors.fname}</Text>
                ) : null}

                <FloatingLabelInput
                  label="Last Name"
                  isFocused={true}
                  value={this.state.lname}
                  onChangeText={text2 => this.setState({lname: text2})}
                  blurOnSubmit
                  testID="lname"
                />
                {errors.lname ? (
                  <Text style={styles.error}>{errors.lname}</Text>
                ) : null}

                <FloatingLabelInput
                  label="Company Email"
                  isFocused={true}
                  value={this.state.c_email}
                  onChangeText={text3 => this.setState({c_email: text3})}
                  blurOnSubmit
                  testID="c_email"
                />
                {errors.c_email ? (
                  <Text style={styles.error}>{errors.c_email}</Text>
                ) : null}
                <FloatingLabelInput
                  label="Company Website URL"
                  isFocused={true}
                  value={this.state.c_url}
                  onChangeText={text4 => this.setState({c_url: text4})}
                  blurOnSubmit
                  // www={true}
                  testID="c_url"
                />
                {errors.c_url ? (
                  <Text style={styles.error}>{errors.c_url}</Text>
                ) : null}
              </View>
              <View>
                <View style={styles.oneButton}>
                  <CustomButton
                    buttonTest="requestButton"
                    buttonBackgroundColor={true}
                    textColor={false}
                    hasShadow={true}
                    isFullWidth={false}
                    viewStyle={largeButton.buttonViewStyle}
                    textStyle={largeButton.buttonTextStyle}
                    handleButtonPress={this.handleNavigate}>
                    Submit Request
                  </CustomButton>
                </View>
              </View>
            </View>
            <View style={styles.terms}>
              <Text style={styles.terms_font}>Terms and Conditions</Text>
            </View>
          </CustomOnboardingScreen>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    width: '100%',
    height: '100%',
  },
  startHeight: {
    height: 128,
  },
  oneButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  requestWidth: {
    width: '100%',
  },
  terms: {
    marginTop: 30,
    marginBottom: 20,
    alignItems: 'center',
  },
  terms_font: {
    color: 'rgb(155,155,155)',
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
  inputs: {
    flex: 1,
    justifyContent: 'space-between',
  },
  error: {
    color: '#C0392B',
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
    marginTop: 6,
  },
});

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {addNewRequest})(RequestScreen);
