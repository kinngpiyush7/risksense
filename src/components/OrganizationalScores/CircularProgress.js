import React, {Component, useState, useEffect} from 'react';
import {Text, View, Animated, Easing} from 'react-native';
import Svg, {Circle, Text as SvgText, TSpan, G} from 'react-native-svg';

const AnimatedCircle = Animated.createAnimatedComponent(Circle);
const colorsArray = [
  'rgb(121,184,79)',
  'rgb(251,206,63)',
  'rgb(255,137,48)',
  'rgb(213,62,79)',
  'rgb(158,10,27)',
];

const Progress = props => {
  // start value of dash offset with circumference of circle
  const dash = new Animated.Value(props.circum);

  // animates dash offset from circumference of circle
  //  to the percentage of the value
  Animated.timing(dash, {
    toValue: props.circum * (1 - props.percent),
    easing: Easing.ease,
    duration: 1000,
  }).start();

  return <AnimatedCircle {...props} strokeDashoffset={dash} />;
};

export default class CircularProgress extends Component {
  state = {
    currentColor: '',
    percentageStroke: '',
    r: '',
    size: '',
    dash: '',
    circumference: '',
    circleStroke: '',
    progressStroke: '',
  };
  componentDidMount() {
    const {value, maxValue, small} = this.props;
    const rad = small ? 17 : 50;

    this.setState({
      r: rad,
      percentageStroke: value / maxValue,
    });
    const percentage = Math.trunc((value / maxValue) * 100);

    // applies color to circular progress bar depending on the values

    if (this.props.hasOwnProperty('positiveIncrease')) {
      const {positiveIncrease} = this.props;
      if (percentage >= 0 && percentage <= 25)
        this.setState({
          currentColor: positiveIncrease ? colorsArray[4] : colorsArray[0],
        });
      else if (percentage >= 26 && percentage <= 45)
        this.setState({
          currentColor: positiveIncrease ? colorsArray[3] : colorsArray[1],
        });
      else if (percentage >= 46 && percentage <= 65)
        this.setState({currentColor: colorsArray[2]});
      else if (percentage >= 66 && percentage <= 85)
        this.setState({
          currentColor: positiveIncrease ? colorsArray[1] : colorsArray[3],
        });
      else if (percentage >= 86)
        this.setState({
          currentColor: positiveIncrease ? colorsArray[0] : colorsArray[4],
        });
    } else {
      this.setState({currentColor: 'rgb(155,155,155)'});
    }
  }
  render() {
    // calculation for circular progress depending on the size
    const r = this.props.small ? 15 : 50;
    const size = this.props.small ? 17 : 54;
    const circumference = 2 * Math.PI * r;
    const circleStroke = this.props.small ? 3.6 : 6.4;
    const progressStroke = this.props.small ? 4 : 8;
    const {percentageStroke} = this.state;

    return (
      <View>
        <Svg style={{width: size * 2, height: size * 2}}>
          <Circle
            cx={size}
            cy={size}
            r={r}
            stroke={this.props.small ? 'rgb(221,221,221)' : 'rgb(243,245,248)'}
            strokeWidth={circleStroke}
            fill="none"
          />
          <Progress
            cx={size}
            cy={size}
            r={r}
            stroke={this.state.currentColor}
            strokeWidth={progressStroke}
            fill="none"
            strokeLinecap="round"
            transform={`rotate(-90,${size},${size})`}
            strokeDasharray={`${circumference}`}
            percent={percentageStroke}
            circum={circumference}
          />
          {this.props.small && (
            <SvgText
              x={r + (progressStroke * 2) / 3}
              y={r + (progressStroke * 3) / 2}
              textAnchor="middle"
              fontSize={11}
              fill="rgb(72,84,101)"
              fontFamily="Poppins-SemiBold">
              {this.props.value}
            </SvgText>
          )}
          {!this.props.small && (
            <G
              x={r + (progressStroke * 2) / 3}
              y={r + (progressStroke * 2) / 10}>
              <SvgText textAnchor="middle">
                {this.props.onlyBenchmark ? (
                  <TSpan
                    x={0}
                    fill={this.state.currentColor}
                    fontSize={16}
                    fontWeight={800}
                    fontFamily="Poppins-Bold">
                    {`${this.props.value}`}
                  </TSpan>
                ) : (
                  <TSpan
                    x={0}
                    fill="rgb(43,43,43)"
                    fontSize={14}
                    fontWeight={800}
                    fontFamily="Poppins-Bold">
                    {`${this.props.value}/${this.props.maxValue}`}
                  </TSpan>
                )}
                <TSpan
                  x={0}
                  y={16}
                  fill="rgb(86,86,86)"
                  fontSize={10}
                  fontWeight={600}
                  fontFamily="Poppins-Medium">
                  Benchmark
                </TSpan>
                <TSpan
                  x={0}
                  y={26}
                  fill="rgb(86,86,86)"
                  fontSize={10}
                  fontWeight={600}
                  fontFamily="Poppins-Medium">
                  Score
                </TSpan>
              </SvgText>
            </G>
          )}
        </Svg>
      </View>
    );
  }
}
