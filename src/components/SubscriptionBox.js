import React from 'react';
import {View, StyleSheet, Text, Image} from 'react-native';
import checkmark from '../assets/images/check-square.png';

const SubcriptionBox = props => {
  return (
    <View testID="SB_box" style={styles.box}>
      <View>
        <View testID="SB_heading" style={styles.heading}>
          <Text testID="SB_heading_text" style={styles.heading_text}>
            {props.heading_text}
          </Text>
        </View>
        <Text testID="SB_price" style={styles.price}>
          {props.price}
        </Text>
        <Text testID="SB_outertable_text1" style={styles.outertable_text1}>
          {props.outertable_text1}
        </Text>
        <Text
          testID="SB_outertable_text1"
          style={[
            props.secondbox
              ? styles.outertable_text2_color
              : styles.outertable_text2,
          ]}>
          {props.outertable_text2}
        </Text>
        <View testID="SB_inner_table" style={styles.inner_table}>
          <View testID="SB_inner_table_head" style={styles.inner_table_head}>
            <Image
              testID="SB_checkmark"
              style={styles.checkmark}
              source={checkmark}></Image>
            <Text
              testID="SB_inner_tableheading"
              style={styles.inner_table_heading}>
              Report Features
            </Text>
          </View>
          <View style={styles.inner_table_texts}>
            <Text style={styles.inner_table_text}>Digital Footprint</Text>
            <Text style={styles.inner_table_text}>Actionable Items</Text>
            <Text style={styles.inner_table_text}>Security Analysis Scope</Text>

            <Text style={styles.inner_table_text}>Network Security</Text>

            <Text style={styles.inner_table_text}>IP Reputation</Text>
            <Text style={styles.inner_table_text}>Patch Cadence</Text>
            <Text style={styles.inner_table_text}>Application Security</Text>
            <Text style={styles.inner_table_text}>Email Security</Text>
            <Text style={styles.inner_table_text}>DNS Security</Text>
            <Text style={styles.inner_table_text}>Appendix</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    borderColor: '#00B3C4',
    width: 314,
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: '#fff',
    padding: 14,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 4,
  },
  heading: {
    borderBottomColor: '#E2E2E2',
    borderRadius: 2,
    borderBottomWidth: 1,
    paddingBottom: 9,
    fontFamily: 'Poppins-SemiBold',
    fontSize: 18,
  },
  price: {
    color: '#00B3C4',
    textAlign: 'center',
    fontSize: 30,
    fontFamily: 'Poppins-SemiBold',
    marginVertical: 1,
  },
  outertable_text1: {
    color: '#2B2B2B',
    textAlign: 'center',
    padding: 3,
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
  },
  outertable_text2: {
    textAlign: 'center',
    padding: 3,
    color: '#565656',
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
  },
  outertable_text2_color: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    padding: 3,
    color: '#3D7C82',
  },
  heading_text: {
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
  },
  inner_table: {
    backgroundColor: '#F7F7F7',
    borderRadius: 5,
    marginTop: 20,
  },

  inner_table_head: {
    backgroundColor: '#F1F1F1',
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 10,
  },
  checkmark: {marginTop: 'auto', marginBottom: 'auto', width: 16, height: 16},
  inner_table_heading: {
    textAlign: 'center',
    fontSize: 13,
    paddingLeft: 6,
    fontFamily: 'Poppins-SemiBold',
  },
  inner_table_texts: {
    fontFamily: 'Poppins-Medium',
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 12,
  },
  inner_table_text: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    padding: 3,
    color: '#565656',
  },
});

export default SubcriptionBox;
