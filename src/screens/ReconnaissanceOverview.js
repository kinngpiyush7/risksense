import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';

import {Table, Row, Rows} from 'react-native-table-component';

import Footer from '../components/Footer';
import backarrow from '../assets/images/icons8-back-30.png';
import CustomButton from '../components/CustomButton';
import recon_overview from '../assets/images/reconOverview/srsIconsReconOverview.png';
import DonutGraph from '../components/DonutGraph';
import ReportTable from '../components/ReportTable';
import {SafeAreaView} from 'react-navigation';
import {mediumButton} from '../assets/styles/Buttons';
import {widthPercentageToDP} from '../components/Responsive';

export default class ReconnaissanceOverview extends Component {
  state = {
    CurrentPage: 1,
    RowPerPage: 10,
  };

  handlePageChange = pageNumber => {
    this.setState({CurrentPage: pageNumber});
  };

  static navigationOptions = {
    header: null,
  };

  navigation = () => {
    this.props.navigation.navigate('SubscriptionPlan');
  };

  render() {
    const data = [
      [
        <Text style={styles.centre}>geo.sha.merck.gov</Text>,
        <Text style={styles.centre}>Merck group</Text>,
        <Text style={styles.centre}>Registered Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>adminaccess.mymdthink.merck.gov</Text>
        </View>,
        <Text style={styles.centre}>See PrivacyGuardian.org</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <View style={styles.fraudulent_style} id="fraud">
          <Text>barcodedt-prod.mdthink.merck.gov</Text>
        </View>,
        <Text style={styles.centre}>Secure Apps LLC</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <Text style={styles.centre}>www.chart.merck.gov</Text>,
        <Text style={styles.centre}>Merck OCE group</Text>,
        <Text style={styles.centre}>Domain</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <Text style={styles.centre}>awrm.sha.merck.gov</Text>,
        <Text style={styles.centre}>Merck Asia</Text>,
        <Text style={styles.centre}>Registered Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>dev2.cp.mdthink.merck.gov</Text>
        </View>,
        <Text style={styles.centre}>NAMECHEAP</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <Text style={styles.centre}>strmr2.sha.merck.gov</Text>,
        <Text style={styles.centre}>care2care.dhmh.merck.</Text>,
        <Text style={styles.centre}>Subdomain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <Text style={styles.centre}>mdvideoportal.chart.merck.gov</Text>,
        <Text style={styles.centre}>autodiscover.goccp.</Text>,
        <Text style={styles.centre}>Domain</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <Text style={styles.centre}>geo.sha.merck.gov</Text>,
        <Text style={styles.centre}>Merck group</Text>,
        <Text style={styles.centre}>Registered Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <Text style={styles.centre}>merck.gov.screen.com</Text>,
        <Text style={styles.centre}>Corporation Merck Limited</Text>,
        <Text style={styles.centre}>SubDomain</Text>,
        <Text style={styles.centre}>192.168.0.26</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>adminrights.premierleague.org</Text>
        </View>,
        <Text style={styles.centre}>Refer watchJesseLombard.com</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],

      [
        <Text style={styles.centre}>icc.renderapp.icu.io</Text>,
        <Text style={styles.centre}>AutoDiscover Asia Limited</Text>,
        <Text style={styles.centre}>Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <Text style={styles.centre}>mdvideoportal.chart.merck.gov</Text>,
        <Text style={styles.centre}>Merck group</Text>,
        <Text style={styles.centre}>Registered Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <Text style={styles.centre}>dev2.cp.mdthink.merck.gov</Text>,
        <Text style={styles.centre}>NAMECHEAP</Text>,
        <Text style={styles.centre}>SubDomain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>adminaccess.mymdthink.merck.gov</Text>
        </View>,
        <Text style={styles.centre}>See PrivacyGuardian.org</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <Text style={styles.centre}>geo.sha.merck.gov</Text>,
        <Text style={styles.centre}>Merck group</Text>,
        <Text style={styles.centre}>Registered Domain</Text>,
        <Text style={styles.centre}>192.185.238.86</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>andre.vue.is</Text>
        </View>,
        <Text style={styles.centre}>groupismvalue.in</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
      [
        <View style={styles.fraudulent_style}>
          <Text>adangeraccess.ss</Text>
        </View>,
        <Text style={styles.centre}>Refer Benedict.co.in</Text>,
        <Text style={styles.fraudulent_value}>Fraudulent</Text>,
        <Text style={styles.centre}>NULL</Text>,
      ],
    ];

    let filteredData = data;
    // [Pagination] Fetching the first and last companies index
    // and slicing to get current companies
    let indexOfLastRow = this.state.CurrentPage * this.state.RowPerPage;
    let indexOfFirstRow = indexOfLastRow - this.state.RowPerPage;
    let current_data =
      filteredData !== null
        ? filteredData.slice(indexOfFirstRow, indexOfLastRow)
        : null;

    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="NS_container" style={styles.NS_container}>
            <View testID="NR_header" style={styles.header}>
              <TouchableOpacity
                testID="NR_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="NR_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <Image
                testID="PR_reportimage"
                style={{marginTop: 10, marginRight: 6}}
                source={recon_overview}></Image>
              <View testID="NR_header_texts" style={styles.header_texts}>
                <Text testID="NR_heading" style={styles.heading}>
                  Reconnaissance Overview
                </Text>
                <Text testID="NR_sub_heading" style={styles.sub_heading}>
                  Digital Footprint is determined by open ports, services and
                  application banners from the data
                </Text>
              </View>
            </View>

            <View testID="NR_graphcontainer" style={styles.NR_graphcontainer}>
              <View
                style={{
                  flexBasis: '25%',
                  paddingTop: 25,
                  paddingBottom: 25,
                  // borderWidth: 1,
                }}>
                <DonutGraph
                  keys={[1, 2, 3, 4]}
                  values={[55, 7, 18, 20]}
                  colors={['#00B3C4', '#C61227', '#6DD9E3', '#0B9AA8']}
                  customStyles={styles.pie_main}
                  outerRadius="100%"
                  innerRadius="70%"
                  text1="166"
                  text2="Total"
                />
              </View>
              <View
                style={{
                  flexBasis: '75%',
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                }}>
                <View
                  style={{
                    flexBasis: '45%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}>
                  <View
                    style={{
                      flexBasis: '75%',
                    }}>
                    <Text style={styles.donut_info}>IP within Domain</Text>
                    <Text style={styles.donut_info}>Domains without IP</Text>
                  </View>
                  <View
                    style={{
                      flexBasis: '15%',
                    }}>
                    <Text style={[styles.donut_info, {color: '#00B3C4'}]}>
                      87
                    </Text>
                    <Text style={[styles.donut_info, {color: '#6DD9E3'}]}>
                      19
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    width: 0.8,
                    backgroundColor: '#E0E0E0',
                    height: 90,
                    alignSelf: 'center',
                  }}
                />
                <View
                  style={{
                    flexBasis: '45%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignSelf: 'center',
                  }}>
                  <View
                    style={{
                      flexBasis: '75%',
                    }}>
                    <Text style={styles.donut_info}>Sub Domains</Text>
                    <Text style={styles.donut_info}>Fraudulent Domains</Text>
                  </View>
                  <View
                    style={{
                      flexBasis: '15%',
                    }}>
                    <Text style={[styles.donut_info, {color: '#0B9AA8'}]}>
                      23
                    </Text>
                    <Text style={[styles.donut_info, {color: '#C61227'}]}>
                      7
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View testID="NR_table" style={styles.NR_table}>
              {filteredData.length > 0 ? (
                <ReportTable
                  showLegend={false}
                  tableMargin={{marginHorizontal: 0}}
                  heading="The asset discovery engine gets company domain as the only
                input. The engine identifies any other domains that belong to
                the same company and then finds the subdomains from VirusTotal,
                PassiveTotal and web search engines."
                  column_names={[
                    'Domain/FQDN',
                    'Registrant Organization',
                    'By Reconnaissance',
                    'IP Address',
                  ]}
                  widthArr={[
                    widthPercentageToDP('30'),
                    widthPercentageToDP('25'),
                    widthPercentageToDP('25'),
                    widthPercentageToDP('14'),
                  ]}
                  column_data={current_data}
                  // passing props required value for pagination just uncomment when connented to databaase
                  RowPerPage={this.state.RowPerPage}
                  totalRows={filteredData.length}
                  handlePageChange={this.handlePageChange}
                  CurrentPage={this.state.CurrentPage}
                />
              ) : (
                <View testID="no-result-found"> No Result Found</View>
              )}
            </View>
            <View
              testID="NR_bottomcontainer"
              style={{
                margin: 20,
                marginBottom: 30,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                testID="NR_bottomcontainer_text"
                style={{flexShrink: 1, width: 500}}>
                <Text
                  testID="NR_bottomcontainer_text_1"
                  style={{fontFamily: 'Poppins-Medium', fontSize: 12}}>
                  Action items to improve the Organization Score -
                  <Text
                    testID="NR_bottomcontainer_text_2"
                    style={{fontFamily: 'Poppins-Regular', fontSize: 12}}>
                    Disable critical services like Redis, MySQL, and RDP
                  </Text>
                </Text>
              </View>
              <View style={{marginLeft: 20}}>
                <CustomButton
                  buttonTest="startButton"
                  buttonBackgroundColor={false}
                  textColor={true}
                  hasShadow={true}
                  navigation={this.props.navigation.navigate}
                  handleButtonPress={this.handleNavigation}
                  textStyle={mediumButton.buttonTextStyle}
                  viewStyle={mediumButton.buttonViewStyle}
                  handleButtonPress={this.navigation}>
                  Get a Deeper Scan Report
                </CustomButton>
              </View>
            </View>
          </View>
          <Footer />
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  NS_container: {
    padding: 15,
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {paddingTop: 10, paddingEnd: 10, paddingRight: 10},
  heading: {
    fontSize: 18,
    paddingBottom: 3,
    fontFamily: 'Poppins-SemiBold',
  },
  sub_heading: {color: '#565656', fontFamily: 'Poppins-Regular', fontSize: 12},
  donut_info: {
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
  },
  table: {
    backgroundColor: '#F7F7F7',
    borderRadius: 4,
    // borderWidth: 1,
    borderColor: '#D2D2D2',
  },
  NR_graphcontainer: {
    flexDirection: 'row',
    width: '70%',
    marginLeft: 'auto',
  },
  pie_main: {
    height: 140,
    transform: [{rotateX: 3}, {rotateZ: -0.3}],
  },
  container: {marginTop: 20},
  head: {
    fontSize: 14,
    paddingLeft: 15,
    // fontFamily: 'Poppins-Medium',
  },
  headtext: {color: '#8C8C8C', fontFamily: 'Poppins-Medium', fontSize: 14},
  datatext: {fontFamily: 'Poppins-Regular', fontSize: 13},
  columndata: {
    borderBottomWidth: 1,
    borderBottomColor: '#E2E2E2',
    paddingVertical: 12,
  },
  centre: {
    paddingLeft: 15,
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
  header_style: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    padding: 10,
  },
  fraudulent_style: {
    borderLeftWidth: 3,
    borderLeftColor: '#E35C6C',
    paddingLeft: 15,
  },
  fraudulent_value: {
    color: '#C61227',
    paddingLeft: 15,
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
  },
});
