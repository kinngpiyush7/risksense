/* eslint-disable no-undef */
describe('New Domain Feature Testing', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });
  it('The NewDomainScreen should be tested along with included Reuseable Components', async () => {
    // const comp_email = 'headstrait@gmail.com';
    // const c_url = 'headstrait.com';
    await waitFor(element(by.id('demo')))
      .toBeVisible()
      .withTimeout(10000);
    await element(by.id('startButton')).tap();
    await element(by.id('requestButton')).tap();
    await expect(element(by.id('thankyou_screen'))).toBeVisible();
    await expect(element(by.id('thankyou_button'))).toBeVisible();
    await element(by.id('thankyou_button')).tap();
    await expect(element(by.id('checkNewDomain'))).toBeVisible();
    await element(by.id('checkNewDomain')).tap();
    await expect(element(by.id('domain_screen'))).toBeVisible();
    await expect(element(by.id('comp_email'))).toBeVisible();
    await element(by.id('comp_email')).tap();
    // await element(by.id('comp_email')).typeText(c_email);
    await device.pressBack();
    // await element(by.id("rest-time")).typeText(c_profile);
    await element(by.id('domain_button')).tap();
  });
});
