import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import SubscriptionBox from '../components/SubscriptionBox';
import backarrow from '../assets/images/icons8-back-30.png';
import Footer from '../components/Footer';
import {SafeAreaView} from 'react-navigation';

export default class SubscriptionPlan extends Component {
  // Removing the Header from the Navigation Navbar
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <SafeAreaView>
        <ScrollView>
          <View testID="SP_Container" style={styles.Container}>
            <View testID="SP_header" style={styles.header}>
              <TouchableOpacity
                testID="SP_backarrow_button"
                style={styles.backarrow}
                onPress={() => {
                  this.props.navigation.navigate('DashBoard');
                }}>
                <Image
                  testID="SP_backarrow"
                  style={styles.backarrow}
                  source={backarrow}></Image>
              </TouchableOpacity>
              <View testID="SP_header_texts" style={styles.header_texts}>
                <Text testID="SP_heading" style={styles.heading}>
                  Select a Scan Report Subscription Plan
                </Text>
                <Text testID="SP_sub_heading" style={styles.sub_heading}>
                  Security rating report each for each scoring component and the
                  overall organizational score
                </Text>
              </View>
            </View>
            <View testID="SP_boxs" style={styles.boxs}>
              <View testID="SP_box1" style={styles.box1}>
                <Text testID="SP_box_header" style={styles.box_header}>
                  Best for small businesses
                </Text>
                {/* calling the reuseable Subscriptionbox component  */}
                <SubscriptionBox
                  price="$99.00"
                  heading_text="Starter"
                  outertable_text1="One Domain"
                  outertable_text2="One time download"
                />
              </View>
              <View testID="SP_box2" style={styles.box2}>
                <Text testID="SP_box_header" style={styles.box_header}>
                  Best for large organizations
                </Text>
                {/* calling the reuseable Subscriptionbox component  */}
                <SubscriptionBox
                  secondbox={true}
                  price="$15,000"
                  heading_text="Enterprise"
                  outertable_text1="Yearly Subscription"
                  outertable_text2="+ $399 per vendor extra"
                />
              </View>
            </View>
            <Footer />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  Container: {
    width: Dimensions.get('window').width,
  },
  header: {flexDirection: 'row', paddingTop: 5},
  backarrow: {padding: 10},
  heading: {fontFamily: 'Poppins-SemiBold', fontSize: 18},
  sub_heading: {fontFamily: 'Poppins-Regular', color: '#565656'},
  boxs: {
    flexDirection: 'row',
    marginTop: 50,
    marginBottom: 68,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  box_header: {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10,
    color: '#2B2B2B',
    fontFamily: 'Poppins-Regular',
  },
  box1: {
    marginRight: 14,
  },
  box2: {
    marginLeft: 14,
  },
});
